<?php
session_start();
include 'cms/core/class/db.class.php';

include 'cms/modules/noticias/define.php';
include 'cms/modules/noticias/model/noticia.php';
include 'cms/modules/noticias/model/noticiaDAO.php';

include 'cms/modules/almanews/define.php';
include 'cms/modules/almanews/model/noticiaDAOext.php';

include 'cms/modules/products/model/product.class.php';
include 'cms/modules/products/model/productDAO.class.php';

$db = new Database();
$db->connect();

if( !isset($_SESSION['ids']) ){
    $array = array();
    $_SESSION['ids'] = serialize($array);
}

$array = unserialize($_SESSION['ids']);

$term = $_GET['term'];
$DAO = new NoticiaDAO($db);
$items = $DAO->getsSearch($term);

$DAO2 = new NoticiaDAOext($db);
$items2 = $DAO2->getsSearch($term);

$pDAO = new ProductDAO($db);
$sql2 = ' products_ref LIKE "%'.$term.'%" OR products_title LIKE "%'.$term.'%" ';
$ps = $pDAO->getsSearch($sql2, "products_id", "asc");

require ('xajax/xajax_core/xajax.inc.php');
$xajax = new xajax();

include 'xajax/funtions/PHPAjaxFunctions.php';
$xajax->registerFunction("add");
$xajax->registerFunction("subCat");
$xajax->processRequest();
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="ie6 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="ie7 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="ie8 lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="ie9"> <!--<![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>VITRAL</title>
    <meta name="keywords" content="Aluminio, Ventanas, Puertas, Perfiles, Perfilería, Vitral, Línea+universal, Corrediza, Corredera, Proyectante, Batiente">
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <?php
    $xajax->printJavascript("xajax/");
    ?>
    <script language="javascript" type="text/javascript" src="js/jquery-1.7.2.min.js"> </script> 
    <!-- CUSTOM SELECT http://jquery.sanchezsalvador.com/page/jquerycombobox.aspx-->
    <script language="javascript" type="text/javascript" src="js/jquery.dd.js"> </script> 
    <link href="css/dd.css" rel="stylesheet" type="text/css">
    <!--js personales -->
    <script language="javascript" type="text/javascript" src="js/vitral.js"> </script> 
    
    <link href="css/vitral.css" rel="stylesheet" type="text/css">
</head>

<body>
<div class="headerWrapper">
	<div class="headderBox">
    	<div class="logoBox"><a href="index.php"><img src="imagenes/logo.png" width="211" height="113" border="0" /></a></div>
        <div class="sloganBox">Perfiles y sistemas<br />con <strong>CALIDAD DE VIDA</strong></div>
        
        <div class="headerMenuBox">
        	<a href="index.php">HOME</a>
        	<a href="quienesSomos.php">QUIÉNES SOMOS</a>
            <a href="clubClientes.php">DESCARGAS</a> 
            <a href="productos.php">PRODUCTOS</a>    
            <a class="selected"  href="beneficios.php">BENEFICIOS Y NOTICIAS</a> 
            <a href="galeria.php">GALERÍA DE IMAGENES</a>
            <div class="logoSegundo"><a href="alma.php"><img src="imagenes/almaLogo.png" width="85" height="46" border="0"  /></a></div>
        </div>
        
        <div class="contactenosBox">
        	<span class="contactenosLeyenda">"Somos el respaldo a su <strong>VENTA</strong>"</span>
            <a href="contactenos.php">CONTÁCTENOS</a>
        </div>
        
        <div class="buscadorBox">
        	<?php include 'busqueda.php';?>
        </div>
    </div>
</div>
<div class="contentWrapper">
	<div class="contentBox">
    	<div class="internasBox">
    		<h1>Resultados para '<?php echo $term;?>'</h1><br />

            <?php $i=0; foreach ($items as $item){   ?>
          	<div class="beneficiosItemBox">
                    <div class="textos" style="width:100%;"><h2><?php echo $item->getTitle();?></h2>
                        <?php echo $item->getDescriptionLimited(200);?>
                    <br /> <br />
                    <a href="beneficiosDetalle.php?id=<?php echo $item->getId();?>" class="vermasNoticias" style="bottom:-10px">VER MÁS</a>
                    </div>
                <div class="clear"></div>
          	</div>
                <?php } ?>

                <?php $i=0; foreach ($items2 as $item){   ?>
          	<div class="beneficiosItemBox">
                    <div class="textos" style="width:100%;"><h2><?php echo $item->getTitle();?></h2>
                        <?php echo $item->getDescriptionLimited(200);?>
                    <br /> <br />
                    <a href="almaDetalle.php?id=<?php echo $item->getId();?>" class="vermasNoticias" style="bottom:-10px">VER MÁS</a>
                    </div>
                <div class="clear"></div>
          	</div>
                <?php } ?>

                <?php $i=0; foreach ($ps as $item){   ?>
          	<div class="beneficiosItemBox">
                    <div class="textos" style="width:100%;"><h2><?php echo $item->getTitle();?></h2>
                        <?php echo $item->getDecription();?>
                    <br /> <br />
                    <div class="agregarCotizacionBox" style="background: #88b2d6; width: 200px; color: white; height: 40px; vertical-align: middle; font-weight: bold; text-align: center;">
                        
                        <input type="checkbox" value="1" name="<?php echo $item->getId();?>" onclick="xajax_add('<?php echo $item->getId();?>');" <?php if(in_array($item->getId(), $array) ) echo 'checked'; ?>  />
                        Agregar a cotización
                    </div>
                    </div>
                <div class="clear"></div>
          	</div>
                <?php } ?>

            <div class="clear"></div>
      	</div>
    </div>
    <div class="contentBelowBg"></div>	
</div>

<?php include('footer.php'); ?>
</body>
<?php include 'analytics.php';?>
</html>