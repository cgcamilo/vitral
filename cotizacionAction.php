<?php
session_start();
include 'cms/core/class/db.class.php';

include 'cms/modules/products/define.php';
include 'cms/modules/products/model/category.php';
include 'cms/modules/products/model/categoryDAO.php';
include 'cms/modules/products/model/subCategoryDAO.php';
include 'cms/modules/products/model/product.class.php';
include 'cms/modules/products/model/productDAO.class.php';

include 'mail.php';

$db = new Database();
$db->connect();

if( !isset($_SESSION['ids']) ){
    $array = array();
    $_SESSION['ids'] = serialize($array);
}


$array = unserialize($_SESSION['ids']);

if( isset($_GET['id']) ){
    unset($array[$_GET['id']]);
    $_SESSION['ids'] = serialize($array);
}

if( count($array) == 0 ){
    $location = "location: ./productos.php?";
    header($location."&message=Tienes que añadir productos si deseas cotizarlos");
    exit;
}

$DAO = new ProductDAO($db);

foreach($_GET as $key => $value){
    if( $value == "" ){
        $location = "location: ./cotizacion.php?";
        header($location."&message=Todos los datos son obligatorios&".$key);
        exit;
    }
    $$key = $value;
}

if( $nombre == 'Nombres' || $apellidos == "Apellidos" || $email == "E-mail" || $empresa == "Empresa"
        || $tel == "Teléfono" || $dir == "Dirección" || $ciudad== "Ciudad"){
    $location = "location: ./cotizacion.php?";
    header($location."&message=Todos los datos son obligatorios");
    exit;
}

if( !ValidaMail($email) ){
    $location = "location: ./cotizacion.php?";
    header($location."&message=Email invalido");
    exit;
}

$body = "<h3>Cotizacion usuario:</h3><br />\n\n";
$body .= "Un usuario ha cotizado unos productos desde la web:.<br />\n";

 foreach( $array as $id ){
    $p = $DAO->getById($id);
    if($p == null) continue;

    $cantidad = $_GET['cantidad'.$p->getId()];

    $body .= "<b>Nombre Producto:</b> ".accents2HTML($p->getTitle())."<br />\n";
    $body .= "<b>Referencia:</b> ".accents2HTML($p->getRef())."<br />\n";
    $body .= "<b>Cantidad de paquetes:</b> ".$cantidad."<br />\n";
 }


$body .= "<br /><h3>Datos cliente:</h3><br />\n";
$body .= "<b>Nombre:</b> ".accents2HTML($nombres)."<br />\n";
$body .= "<b>Apellidos:</b> ".accents2HTML($apellidos)."<br />\n";
$body .= "<b>Email:</b> ".accents2HTML($email)."<br />\n";
$body .= "<b>Empresa:</b> ".accents2HTML($empresa)."<br />\n";
$body .= "<b>Telefono:</b> ".accents2HTML($tel)."<br />\n";
$body .= "<b>Direccion:</b> ".accents2HTML($dir)."<br />\n";
$body .= "<b>Ciudad:</b> ".accents2HTML($ciudad)."\n";

$mail2 = new sendCMail("anunez@vitral.com.co", "system@imaginamos.com", "Cotizacion Vitral", $body, "text/html");
$mail2->sendMail();

unset ($_SESSION['ids']);

$location = "location: ./productos.php?";
header($location."&message=Gracias, pronto nos pondremos en contacto");
exit;
?>