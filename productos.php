<?php
session_start();
include 'cms/core/class/db.class.php';

include 'cms/modules/products/define.php';
include 'cms/modules/products/model/category.php';
include 'cms/modules/products/model/categoryDAO.php';
include 'cms/modules/products/model/subCategoryDAO.php';
include 'cms/modules/products/model/product.class.php';
include 'cms/modules/products/model/productDAO.class.php';


$db = new Database();
$db->connect();

if( !isset($_SESSION['ids']) ){
    $array = array();
    $_SESSION['ids'] = serialize($array);
}

$array = unserialize($_SESSION['ids']);
//cats
$catDAO = new categoryDAO($db);
$cats = $catDAO->gets("products_cat_title", "asc");

$subCatDAO = new SubCategoryDAO($db);

//productos
$itemsPerPage = 15;
$page = 0;
if( isset($_GET['page']) )
    $page = $_GET['page'];


$DAO = new ProductDAO($db);

$getBack = '';
foreach ($_REQUEST as $key => $value){
    if( $key == 'page' )
        continue;
    $getBack .= $key.'='.$value;
}

if( !isset($_GET['s']) ){
    $products = $DAO->gets("products_id", "desc", $page*$itemsPerPage, $itemsPerPage);
    $total = $DAO->total();
}else{
    //TODO hacer busqueda...
    if( isset($_GET['ref']) && $_GET['ref'] != 'REF.' ){
        $ref = $_GET['ref'];
        $sql2 = 'products_ref LIKE "%'.$ref.'%"';
        $_GET['message2'] = 'Productos con referencia "'.$ref.'"';
        $gets = 's=1&ref='.$ref;
    }else{
        if(isset($_GET['nombre']) && $_GET['nombre'] != 'NOMBRE'){
            $nombre = $_GET['nombre'];
            $sql2 = ' products_title LIKE "%'.$nombre.'%" ';
            $_GET['message2'] = 'Productos con nombre "'.$nombre.'"';
            $gets = 's=1&nombre='.$nombre;
        }else{
            if(isset($_GET['cat']) && $_GET['cat'] != '0'){
                $catId = $_GET['cat'];
                $catSelected = $catDAO->getById($catId);
                //2 opciones
                if($_GET['subcat'] == '0'){
                    //1 solo cat
                    $sql2 = ' products_id_cat = "'.$catId.'" ';
                    $_GET['message2'] = 'Productos de la categoría "'.$catSelected->getTitle().'"';
                    $gets = 's=1&cat='.$catId.'&subcat=0';
                }else{
                    //2 con subcat
                    $subcatId = $_GET['subcat'];
                    $subCatSelected = $subCatDAO->getById($subcatId);
                    $sql2 = ' products_id_cat = "'.$catId.'" AND products_id_subcat = "'.$subcatId.'" ';
                    $_GET['message2'] = 'Productos de la categoría "'.$catSelected->getTitle().'" y subcategorá "'.$subCatSelected->getTitle().'"';
                    $gets = 's=1&cat='.$catId.'&subcat='.$subcatId;
                }
            }
        }
    }

    $productsC = $DAO->getsSearch($sql2, "products_id", "asc");
    $products = $DAO->getsSearch($sql2, "products_id", "asc", $page*$itemsPerPage, $itemsPerPage);
    $total = count($productsC);
}

$totalPages = (int)($total/$itemsPerPage);
if( ($total%$itemsPerPage) != 0)
        $totalPages++;

$pageNext = $page + 1;
$pagePrev = $page - 1;
if( $pageNext >= $totalPages )
    $pageNext = 0;
if( $pagePrev < 0 )
    $pagePrev = 0;


/*
 * XAJAX
 */
require ('xajax/xajax_core/xajax.inc.php');
$xajax = new xajax();

include 'xajax/funtions/PHPAjaxFunctions.php';
$xajax->registerFunction("add");
$xajax->registerFunction("subCat");
$xajax->processRequest();
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="ie6 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="ie7 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="ie8 lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="ie9"> <!--<![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>VITRAL</title>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <!-- XAJAX -->
    <?php
    $xajax->printJavascript("xajax/");
    ?>
    <meta name="keywords" content="Aluminio, Ventanas, Puertas, Perfiles, Perfilería, Vitral, Línea+universal, Corrediza, Corredera, Proyectante, Batiente">
    <script language="javascript" type="text/javascript" src="js/jquery-1.7.2.min.js"> </script> 
    <!-- CUSTOM SELECT http://jquery.sanchezsalvador.com/page/jquerycombobox.aspx-->
    <script language="javascript" type="text/javascript" src="js/jquery.dd.js"> </script> 
    <link href="css/dd.css" rel="stylesheet" type="text/css">
    <!-- colorbox -->
    <script language="javascript" type="text/javascript" src="js/jquery.colorbox.js"> </script> 
    <link href="css/colorbox.css" rel="stylesheet" type="text/css">
    <!--js personales -->
    <script language="javascript" type="text/javascript" src="js/vitral.js"> </script> 
    <script>
	$(document).ready(function() {
		/*CONTÁCTENOS*/
		$(".customSelect2").msDropDown({mainCSS:"dd2"});
		$(".prodLight").colorbox();
    });
    function selectInit(){
        $(".customSelect2").msDropDown({mainCSS:"dd2"});
    }
	</script>
    <link href="css/vitral.css" rel="stylesheet" type="text/css">
</head>

<body>
<div class="headerWrapper">
	<div class="headderBox">
    	<div class="logoBox"><a href="index.php"><img src="imagenes/logo.png" width="211" height="113" border="0" /></a></div>
        <div class="sloganBox">Perfiles y sistemas de aluminio<br />con <strong>CALIDAD DE VIDA</strong></div>
        
        <div class="headerMenuBox">
        	<a href="index.php">HOME</a>
        	<a href="quienesSomos.php">QUIÉNES SOMOS</a>
            <a href="clubClientes.php">DESCARGAS</a> 
            <a class="selected" href="productos.php">PRODUCTOS</a>    
            <a href="beneficios.php">BENEFICIOS Y NOTICIAS</a> 
            <a href="galeria.php">GALERÍA DE IMAGENES</a>
            <div class="logoSegundo"><a href="alma.php"><img src="imagenes/almaLogo.png" width="85" height="46" border="0"  /></a></div>
        </div>
        
        <div class="contactenosBox">
        	<span class="contactenosLeyenda">"Somos el respaldo a su <strong>VENTA</strong>"</span>
            <a href="contactenos.php">CONTÁCTENOS</a>
        </div>
        
        <div class="buscadorBox">
        	<?php include 'busqueda.php';?>
        </div>
    </div>
</div>
<div class="contentWrapper">
	<div class="contentBox">
    	<div class="internasBox" style="position:relative">
    		<h1>PRODUCTOS</h1><br />
                <span style="color: red">
                    <?php echo $_GET['message'];?>
                </span>
                <span style="color: #383F46;">
                    <?php //echo $_GET['message2'];?>
                </span>
            <div class="verCotizacionBox">
                <div class="numerodeItems"><span id="totalSpan"><?php echo count($array); ?> productos cotizados</span></div>
                <a href="cotizacion.php" class="enviarCotizacion">VER Y ENVIAR COTIZACIÓN</a>
            </div>
            
            <div class="filtrarProductosBox">
                <form method="get" action="productos.php">
                    <select class="customSelect2" style="width:210px" name="cat" onchange="xajax_subCat(cat.options[cat.options.selectedIndex].value);return false;">
                        <option value="0">CATEGORÍA</option>
                            <?php foreach($cats as $cat){ ?>
                            <option value="<?php echo $cat->getId();?>"  <?php if($_GET['cat'] == $cat->getId()) echo 'selected'; ?>>
                                <?php echo $cat->getTitle();?>
                            </option>
                            <?php } ?>
                     </select>
                    <span id="subsSpan">
                        <select class="customSelect2" style="width:210px" name="subcat">
                            <option value="0">SUBCATEGORÍA</option>
                            <?php
                            if( isset($_GET['cat']) ){
                                $subCats = $subCatDAO->getsByCat($_GET['cat'], "products_subcat_title", "asc");
                                foreach($subCats as $sub){
                            ?>
                            <option value="<?php echo $sub->getId();?>" <?php if($_GET['subcat'] == $sub->getId()) echo 'selected'; ?>>
                                <?php echo $sub->getTitle();?>
                            </option>
                            <?php }} ?>
                        </select>
                    </span>
                    <?php $refV = 'REF.'; if(isset($_GET['ref']) && $_GET['ref'] != "" ) $refV = $_GET['ref']; ?>
                    <?php $nombreV = 'NOMBRE'; if(isset($_GET['nombre']) && $_GET['nombre'] != "" ) $nombreV = $_GET['nombre']; ?>
                    <input type="text" value="<?php echo $refV;?>" name="ref" onfocus="this.value=''" />
                    <input type="text" value="<?php echo $nombreV;?>" name="nombre" onfocus="this.value=''" />
                    <input type="submit" value="" name="s" />
                </form>
            </div>
            
            <div class="productosBox">
                <?php foreach($products as $product){?>
            	<div class="productosItem">
                    <a class="prodLight" href="cms/modules/products/files/<?php echo $product->getImg1();?>"><img src="cms/modules/products/files/<?php echo $product->getImg1();?>" width="163" height="190" border="0" alt="" /></a>
                    <div class="texto">
                        <span class="tituloProducto"><?php echo $product->getTitle();?></span><br />
                        <?php echo $product->getDecription();?><br />
                        <span class="strong">REFERENCIA:</span>
                        <span class="strongRef"><?php echo $product->getRef();?></span>
                    </div>
                    <div class="agregarCotizacionBox">
                        <div class="iconCheck">
                            <input type="checkbox" value="1" name="<?php echo $product->getId();?>" onclick="xajax_add('<?php echo $product->getId();?>');" <?php if(in_array($product->getId(), $array) ) echo 'checked'; ?>  />
                        </div>
                        <div class="texto"> Agregar a cotización</div>
                    </div>
                </div>
                <?php } ?>
            </div>
            
            <div class="clear"></div>
            <div class="paginadorBox">
            	<a href="productos.php?page=<?php echo $pagePrev;?>&<?php echo $gets;?>" class="flechas"> &lt;&lt; ANTERIOR </a>&nbsp;&nbsp;
                <?php
                    $limitPageTop = 20;
                    $limitPage = 15;
                    if( $totalPages > $limitPageTop && ($page > $limitPage) )
                            echo ' <b style="color:#7a7978">...</b>';
                    for($i = 0; $i<$totalPages; $i++){
                    $is = $i+1;
                    if($page == $i){
                        echo '&nbsp;<a href="#" class="number selected">'.$is.'</a>';
                    }
                    else{
                        if( $totalPages > $limitPageTop && ($i-$limitPage > $page || $i+$limitPage < $page ) )
                            continue;
                        echo '&nbsp;<a href="productos.php?page='.$i.'&'.$gets.'"  class="number">'.$is.'</a>';
                    }
                }

                if( $totalPages > 10 && ($page+6 < $totalPages) )
                            echo ' <b style="color:#7a7978">...</b>';
                ?>
            	&nbsp;&nbsp;<a href="productos.php?page=<?php echo $pageNext;?>&<?php echo $gets;?>" class="flechas"> SIGUIENTE &gt;&gt; </a>
            </div>
            <div class="clear"></div>
      	</div>
    </div>
    <div class="contentBelowBg"></div>	
</div>

<?php include('footer.php'); ?>
</body>
<?php include 'analytics.php';?>
</html>
