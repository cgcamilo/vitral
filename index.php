<?php
session_start();
include 'cms/core/class/db.class.php';

include 'cms/modules/banners/define.php';
include 'cms/modules/banners/model/link.class.php';
include 'cms/modules/banners/model/linkDAO.class.php';

include 'cms/modules/home/define.php';
include 'cms/modules/home/model/aspect.class.php';
include 'cms/modules/home/model/aspectDAOHome.php';

include 'cms/modules/encuesta/model/encuesta.php';
include 'cms/modules/encuesta/model/encuestaDAO.php';
include 'cms/modules/encuesta/model/encuestaLinea.php';
include 'cms/modules/encuesta/model/encuestaLineaDAO.php';

include 'cms/modules/clientes/define.php';
include 'cms/modules/clientes/model/linkDAOClientes.php';


$db = new Database();
$db->connect();

$bannerDAO = new LinkDAO($db, 2);
$banners = $bannerDAO->gets("order", "asc");

$clientesDAO = new LinkDAOClientes($db, 2);
$clientes = $clientesDAO->gets("id", "asc");

$homeDAO = new AspectDAOHome($db);
$dtitulo = $homeDAO->getByName('Destacado Título');
$dcuerpo = $homeDAO->getByName('Destacado Cuerpo');
$durl = $homeDAO->getByName('Destacado URL');
$dimagen = $homeDAO->getByName('Destacado Imagen');
$video = $homeDAO->getByName('Video');

$encuestaDAO = new encuestaDAO($db);
$encuestas = $encuestaDAO->gets("id", "desc");
if( count($encuestas) == 0 ){
    $encuesta = new encuesta();
    $encuesta->setId(0);
}
else
    $encuesta = $encuestas[0];
$preguntas = explode("*", $encuesta->getPosibilidades());


if( isset($_GET['encuesta']) && !isset($_SESSION['encuesta']) ){
    $respuesta = $_GET['respuesta'];
    $id = $_GET['id'];

    $encuestaLineaDAO = new encuestaLineaDAO($db);
    $encuestaLinea = new encuestaLinea();
    $encuestaLinea->setIdEncuesta($id);
    $encuestaLinea->setPosibilidad($respuesta);
    $encuestaLineaDAO->save($encuestaLinea);
    $_SESSION['encuesta'] = 1;
}
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="ie6 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="ie7 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="ie8 lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="ie9"> <!--<![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>VITRAL</title>

    <meta name="keywords" content="Aluminio, Ventanas, Puertas, Perfiles, Perfilería, Vitral, Línea+universal, Corrediza, Corredera, Proyectante, Batiente">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    
    <script language="javascript" type="text/javascript" src="js/jquery-1.7.2.min.js"> </script> 
    <!-- CUSTOM SELECT http://jquery.sanchezsalvador.com/page/jquerycombobox.aspx-->
    <script language="javascript" type="text/javascript" src="js/jquery.dd.js"> </script> 
    <link href="css/dd.css" rel="stylesheet" type="text/css">
    <!--js personales -->
    <script language="javascript" type="text/javascript" src="js/vitral.js"> </script> 
    
    <link href="css/vitral.css" rel="stylesheet" type="text/css">
</head>

<body>
<div class="headerWrapper">
	<div class="headderBox">
    	<div class="logoBox"><a href="index.php"><img src="imagenes/logo.png" width="211" height="113" border="0" /></a></div>
        <div class="sloganBox">Perfiles y sistemas de aluminio<br />con <strong>CALIDAD DE VIDA</strong></div>
        
        <div class="headerMenuBox">
        	<a class="selected" href="index.php">HOME</a>
        	<a href="quienesSomos.php">QUIÉNES SOMOS</a>
            <a href="clubClientes.php">DESCARGAS</a> 
            <a href="productos.php">PRODUCTOS</a>    
            <a href="beneficios.php">BENEFICIOS Y NOTICIAS</a> 
            <a href="galeria.php">GALERÍA DE IMAGENES</a>
            <div class="logoSegundo"><a href="alma.php"><img src="imagenes/almaLogo.png" width="85" height="46" border="0"  /></a></div>
        </div>
        
        <div class="contactenosBox">
        	<span class="contactenosLeyenda">"Somos el respaldo a su <strong>VENTA</strong>"</span>
            <a href="contactenos.php">CONTÁCTENOS</a>
        </div>
        
        <div class="buscadorBox">
        	<?php include 'busqueda.php';?>
        </div>
    </div>
</div>
<div class="contentWrapper">
	<div class="contentBox">
    
    <div class="sliderWrapper">
    	<div class="sliderBox">
            <?php foreach($banners as $banner){ ?>
            <div class="sliderItem">
                <img src="cms/modules/banners/files/<?php echo $banner->getImage();?>" width="741" height="266" alt="" />
                <div class="leyenda"><?php echo $banner->getTitle();?></div>
            </div>
            <?php } ?>
     	</div>
    </div>
    
    <div class="mediaBox">
    	<div class="mediaItem">
        	<div class="titulo" style="background-image:url(imagenes/destacadosIcono.png)">DESTACADOS</div>
                <img src="cms/modules/home/files/<?php echo $dimagen->getValue();?>" width="295" height="120" />
            <div class="colLeft">
            <strong>
                <?php echo $dtitulo->getValue();?>
            </strong>
            <br />
            <?php echo $dcuerpo->getValue();?>
            </div>
            <a href="http://<?php echo $durl->getValue();?>" class="vermas">VER MÁS</a>
        </div>
    	<div class="mediaItem">
        	<div class="titulo" style="background-image:url(imagenes/galeriaIcono.png)">GALERIA DE VIDEO</div>
            <iframe class="videoFrame" width="295" height="221" src="http://www.youtube.com/embed/<?php echo $video->getValue();?>" frameborder="0" allowfullscreen></iframe>
        </div>
        <a name="encuesta"></a>
    	<div class="mediaItem">
            <div class="titulo" style="background-image:url(imagenes/encuestaIcono.png)">ENCUESTA</div>
            <div class="textEncuesta"><?php echo $encuesta->getPregunta();?></div>
            <form method="get" action="index.php#encuesta">
            <div class="colLeft">
                <?php $j=0;  foreach($preguntas as $pregunta){ ?>
                <input name="respuesta" type="radio" value="<?php echo $pregunta;?>" <?php if($j==0)echo "checked"; ?> />
                <?php echo $pregunta;?>
                <br />
                <?php $j++; } ?>
                <input type="hidden" name="id" value="<?php echo $encuesta->getId();?>" />
                <br /><br />
                <input type="submit" value="ENVIAR" class="vermas" name="encuesta"  />
            </div>
            </form>
            <?php if(isset($_SESSION['encuesta'])) echo '<div class="textEncuesta" style=" display:block">Gracias por su respuesta</div>'; ?>
        </div>
    </div>
    
    <div class="cadenaValorWrapper">
    	<div class="titulo">SU CADENA  DE VALOR</div>
      <div class="logosBox">
          <?php foreach($clientes as $cliente){ ?>
          <div class="logoItem"><img src="cms/modules/clientes/files/<?php echo $cliente->getImage();?>" height="66" /></div>
          <?php } ?>
        </div>
    </div>
    </div>
    <div class="contentBelowBg"></div>	
</div>

<?php include('footer.php'); ?>
</body>
<?php include 'analytics.php';?>
</html>
