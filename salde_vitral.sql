-- phpMyAdmin SQL Dump
-- version 3.5.5
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 16-01-2013 a las 09:48:32
-- Versión del servidor: 5.0.96-community
-- Versión de PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `salde_vitral`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_alma`
--

CREATE TABLE IF NOT EXISTS `cms_alma` (
  `alma_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `alma_title` varchar(80) default NULL COMMENT 'alma title',
  `alma_description` varchar(120) default NULL COMMENT 'alma description',
  `alma_value` text COMMENT 'field value',
  `alma_type` varchar(20) default NULL COMMENT 'alma type',
  PRIMARY KEY  (`alma_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `cms_alma`
--

INSERT INTO `cms_alma` (`alma_id`, `alma_title`, `alma_description`, `alma_value`, `alma_type`) VALUES
(1, 'Recuadro 1 título', 'Máximo 30 carácteres', 'ALUMINIO', 'INPUT'),
(2, 'Recuadro 2 título', 'Máximo 30 carácteres', 'LAMINADOS', 'INPUT'),
(3, 'Recuadro 3 título', 'Máximo 30 carácteres', 'MAQUINARIA', 'INPUT'),
(4, 'Recuadro 1 Imagen', '220 x 260 px', '51RV.albumesFotoImg.png', 'IMAGE'),
(5, 'Recuadro 2 Imagen', '220 x 260 px', 'HPG9.albumesFotoImg.png', 'IMAGE'),
(6, 'Recuadro 3 Imagen', '220 x 260 px', '7WDZ.albumesFotoImg.png', 'IMAGE'),
(7, 'Datos introductorios', 'Máximo 500 carácteres', 'Gracias a nuestros clientes, quienes han sido motor de crecimiento e innovación, hemos creado ALMA', 'TEXTAREA'),
(8, 'Datos de Contacto', 'Máximo 4 lineas', 'Dirección: Cra 56 No 79 - 55\r\nTel: 571 - 6303394\r\nCelular: 571 - 3002853802\r\nBogotá - Colombia', 'TEXTAREA'),
(9, 'Recuadro 4 Imagen', '220 x 260 px', '3KKH.albumesFotoImg.png', 'IMAGE'),
(10, 'Recuadro 4 título', 'Máximo 30 carácteres', 'ACCESORIOS', 'INPUT');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_almanews`
--

CREATE TABLE IF NOT EXISTS `cms_almanews` (
  `almanews_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `almanews_title` varchar(80) default NULL COMMENT 'NOTICIA title',
  `almanews_date` int(11) unsigned default '0' COMMENT 'NOTICIA fecha timestamp',
  `almanews_img1` varchar(100) default NULL COMMENT 'IMAGEN 1',
  `almanews_img2` varchar(100) default NULL COMMENT 'IMAGEN 1',
  `almanews_order` int(5) unsigned default '0' COMMENT 'NOTICIA order',
  `almanews_description` text COMMENT 'NOTICIA descripcion',
  `almanews_shortdescription` text COMMENT 'NOTICIA descripcion corta',
  `almanews_lang` varchar(100) default 'es' COMMENT 'Idioma',
  PRIMARY KEY  (`almanews_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `cms_almanews`
--

INSERT INTO `cms_almanews` (`almanews_id`, `almanews_title`, `almanews_date`, `almanews_img1`, `almanews_img2`, `almanews_order`, `almanews_description`, `almanews_shortdescription`, `almanews_lang`) VALUES
(1, 'Perfiles de Aluminio', 1339398000, '4VQ2.contact.jpg', '', 1, 'Sistemas de aluminio, para proyectos arquitectónicos modernos, bajo estrictos procesos de calidad, asegurando sus prestaciones y su funcionamiento.\r\n* Ventanas, corredizas, fijas y batientes\r\n* Puertas, corredizas y batientes\r\n* Fachadas flotantes', '', ''),
(2, 'Láminas de Aluminio', 1325404800, 'GDCL.Tree.jpg', '', 1, 'Hojas y rollos de aluminio para proyectos arquitectónicos e industriales.\r\n* Pisos y paredes\r\n* Cubiertas\r\n* Carrocerías\r\n* Decoración en interiores', '', ''),
(3, 'Láminas de Vidrio', 1325404800, 'BHN4.Chrysanthemum.jpg', '', 1, 'Laminas de vidrio flotado, crudo y laminado de calidad, recomendado para el sector de la construcción en usos exteriores e interiores.\r\n* Fachadas flotantes\r\n* Ventanas y puertas, corredizas o batientes\r\n* Diseños interiores', '', ''),
(4, 'Maquinaria', 1325404800, '', '', 1, 'Equipos industriales para el mecanizado de perfiles de aluminio, como centros de maquinado, retesteadora, copiadoras, maquinas de corte, asegurando la calidad en la producción.', '', ''),
(5, 'Accesorios', 1325404800, '', '', 1, 'Empaques de precisión en EPDM, diseñados a la medida para asegurar el vidrio o anjeo, de los sistemas, tanto al interior como al exterior de los proyectos arquitectónicos.   \r\n\r\nImpermeabilizantes y sellantes resistentes a la intemperie y a los rayos UV, con excelente adherencia para los movimientos por dilataciones térmicas, cargas', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_almanews_config`
--

CREATE TABLE IF NOT EXISTS `cms_almanews_config` (
  `almanews_config_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `almanews_config_funcionality` int(1) NOT NULL COMMENT 'Funcionalidad del módulo',
  `almanews_config_date` datetime default NULL COMMENT 'Fecha y hora de instalación módulo',
  PRIMARY KEY  (`almanews_config_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `cms_almanews_config`
--

INSERT INTO `cms_almanews_config` (`almanews_config_id`, `almanews_config_funcionality`, `almanews_config_date`) VALUES
(1, 0, '2012-09-27 12:33:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_alma_config`
--

CREATE TABLE IF NOT EXISTS `cms_alma_config` (
  `alma_config_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `alma_config_funcionality` int(1) NOT NULL COMMENT 'Permitir añadir mas campos dinamicos',
  `alma_config_date` datetime default NULL COMMENT 'Fecha y hora de instalación módulo',
  PRIMARY KEY  (`alma_config_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `cms_alma_config`
--

INSERT INTO `cms_alma_config` (`alma_config_id`, `alma_config_funcionality`, `alma_config_date`) VALUES
(1, 0, '2012-09-27 11:04:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_banners`
--

CREATE TABLE IF NOT EXISTS `cms_banners` (
  `banners_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `banners_title` varchar(120) default NULL COMMENT 'Link title',
  `banners_url` varchar(120) default NULL COMMENT 'Link URL',
  `banners_blank` tinyint(1) default '0' COMMENT 'Open in new blank page',
  `banners_order` int(5) unsigned default '0' COMMENT 'Link order',
  `banners_image` varchar(120) default NULL COMMENT 'Link image',
  `banners_description` text COMMENT 'Link description',
  PRIMARY KEY  (`banners_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `cms_banners`
--

INSERT INTO `cms_banners` (`banners_id`, `banners_title`, `banners_url`, `banners_blank`, `banners_order`, `banners_image`, `banners_description`) VALUES
(1, 'Perfiles de Aluminio con Calidad de Vida', '', 0, 5, 'WUDI.sliderImg1.png', ''),
(2, 'Grandes proyectos', '', 0, 5, '6WC3.refous.1def.jpg', ''),
(3, 'Calidad certificada', '', 0, 5, 'FXJ2.Home.1..perfilesdef.jpg', ''),
(4, 'Producción estandarizada', '', 0, 4, 'LJRW.plantadef.jpg', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_banners_config`
--

CREATE TABLE IF NOT EXISTS `cms_banners_config` (
  `banners_config_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `banners_config_funcionality` int(1) NOT NULL COMMENT 'Funcionalidad del módulo',
  `banners_config_date` datetime default NULL COMMENT 'Fecha y hora de instalación módulo',
  PRIMARY KEY  (`banners_config_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `cms_banners_config`
--

INSERT INTO `cms_banners_config` (`banners_config_id`, `banners_config_funcionality`, `banners_config_date`) VALUES
(1, 2, '2012-09-27 06:51:24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_clientes`
--

CREATE TABLE IF NOT EXISTS `cms_clientes` (
  `clientes_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `clientes_title` varchar(120) default NULL COMMENT 'Link title',
  `clientes_url` varchar(120) default NULL COMMENT 'Link URL',
  `clientes_blank` tinyint(1) default '0' COMMENT 'Open in new blank page',
  `clientes_order` int(5) unsigned default '0' COMMENT 'Link order',
  `clientes_image` varchar(120) default NULL COMMENT 'Link image',
  `clientes_description` text COMMENT 'Link description',
  PRIMARY KEY  (`clientes_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `cms_clientes`
--

INSERT INTO `cms_clientes` (`clientes_id`, `clientes_title`, `clientes_url`, `clientes_blank`, `clientes_order`, `clientes_image`, `clientes_description`) VALUES
(1, 'cliente 1', '', 0, 1, 'IX4L.cadenaLogo1.png', ''),
(2, 'Corpesa', '', 0, 1, '4RI5.cadenaLogo2.png', ''),
(3, 'Cliente 3', '', 0, 1, 'CKE1.cadenaLogo3.png', ''),
(4, 'Cliente 4', '', 0, 1, 'NFEU.cadenaLogo4.png', ''),
(5, 'Cedal aluminio', '', 0, 1, 'UGJ8.Logo Cedal.png', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_clientes_config`
--

CREATE TABLE IF NOT EXISTS `cms_clientes_config` (
  `clientes_config_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `clientes_config_funcionality` int(1) NOT NULL COMMENT 'Funcionalidad del módulo',
  `clientes_config_date` datetime default NULL COMMENT 'Fecha y hora de instalación módulo',
  PRIMARY KEY  (`clientes_config_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `cms_clientes_config`
--

INSERT INTO `cms_clientes_config` (`clientes_config_id`, `clientes_config_funcionality`, `clientes_config_date`) VALUES
(1, 2, '2012-09-27 09:34:20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_configuration`
--

CREATE TABLE IF NOT EXISTS `cms_configuration` (
  `config_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `config_date` datetime default NULL COMMENT 'Fecha y hora de instalacion',
  `config_path` varchar(256) default NULL COMMENT 'Path general de instalacion',
  `config_web` varchar(120) default NULL,
  `config_mail_remitent` varchar(120) NOT NULL,
  `config_company` varchar(120) NOT NULL,
  PRIMARY KEY  (`config_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `cms_configuration`
--

INSERT INTO `cms_configuration` (`config_id`, `config_date`, `config_path`, `config_web`, `config_mail_remitent`, `config_company`) VALUES
(1, '2012-07-25 12:10:42', 'http://saldefrutas.com/Vitral/cms/', 'http://saldefrutas.com/Vitral/', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_encuesta`
--

CREATE TABLE IF NOT EXISTS `cms_encuesta` (
  `encuesta_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `encuesta_pregunta` varchar(200) default NULL COMMENT 'encuesta title',
  `encuesta_posibilidad` text COMMENT 'encuesta options',
  `encuesta_fecha` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`encuesta_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `cms_encuesta`
--

INSERT INTO `cms_encuesta` (`encuesta_id`, `encuesta_pregunta`, `encuesta_posibilidad`, `encuesta_fecha`) VALUES
(1, '¿Nuestro sistema Astral 1.7, cuenta con referencias básicas y referencias opcionales, cuantas son básicas y cuantas opcionales?', ' 5 básicas y 6 opcionales*\r\n 7 básicas y 8 opcionales*\r\n 7 básicas y 6 opcionales*\r\n 5 básicas y 8 opcionales', '2012-09-27 17:53:42'),
(5, 'El marco de la ventana corrediza Astral 1.8 está compuesto por:', 'Cuatro perfiles*Tres perfiles*Dos perfiles*Un solo perfil', '2012-10-24 16:49:12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_encuestalinea`
--

CREATE TABLE IF NOT EXISTS `cms_encuestalinea` (
  `encuestalinea_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `encuestalinea_idEncuesta` int(11) unsigned NOT NULL COMMENT 'encuesta linea',
  `encuestalinea_Posibilidad` text COMMENT 'encuesta linea',
  PRIMARY KEY  (`encuestalinea_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Volcado de datos para la tabla `cms_encuestalinea`
--

INSERT INTO `cms_encuestalinea` (`encuestalinea_id`, `encuestalinea_idEncuesta`, `encuestalinea_Posibilidad`) VALUES
(1, 1, '\r\n 7 básicas y 8 opcionales'),
(2, 1, ' 5 básicas y 6 opcionales'),
(3, 3, 'Tres perfiles x 0'),
(4, 3, 'Tres perfiles x 0'),
(5, 3, 'Cuatro perfiles x 0'),
(6, 4, 'Tres perfiles'),
(7, 4, 'Un solo perfil'),
(8, 4, 'Cuatro perfiles'),
(9, 4, 'Cuatro perfiles'),
(10, 4, 'Cuatro perfiles'),
(11, 4, 'Dos perfiles'),
(12, 4, 'Cuatro perfiles'),
(13, 4, 'Dos perfiles'),
(14, 4, 'Cuatro perfiles'),
(15, 4, 'Un solo perfil'),
(16, 4, 'Cuatro perfiles'),
(17, 4, 'Cuatro perfiles'),
(18, 5, 'Dos perfiles');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_encuesta_config`
--

CREATE TABLE IF NOT EXISTS `cms_encuesta_config` (
  `encuesta_config_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `encuesta_config_funcionality` int(1) NOT NULL COMMENT 'Permitir añadir mas campos dinamicos',
  `encuesta_config_date` datetime default NULL COMMENT 'Fecha y hora de instalación módulo',
  PRIMARY KEY  (`encuesta_config_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `cms_encuesta_config`
--

INSERT INTO `cms_encuesta_config` (`encuesta_config_id`, `encuesta_config_funcionality`, `encuesta_config_date`) VALUES
(1, 0, '2012-09-27 07:53:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_galeria`
--

CREATE TABLE IF NOT EXISTS `cms_galeria` (
  `galeria_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `galeria_title` varchar(80) default NULL COMMENT 'title',
  `galeria_file` varchar(120) default NULL COMMENT 'file image',
  `galeria_id_cat` int(11) unsigned default NULL COMMENT 'category',
  PRIMARY KEY  (`galeria_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `cms_galeria`
--

INSERT INTO `cms_galeria` (`galeria_id`, `galeria_title`, `galeria_file`, `galeria_id_cat`) VALUES
(3, 'prueba 1', 'MNP5.torqlite.jpg', 4),
(5, 'Edificio', 'PS7R.galeria1.jpg', 1),
(6, 'test', '4P74.colombia.jpg', 3),
(7, 'Gatito', '2AEZ.42925002668916135_1yGwSyxl_c.jpg', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_galeria_cats`
--

CREATE TABLE IF NOT EXISTS `cms_galeria_cats` (
  `galeria_cats_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `galeria_cats_name` varchar(80) default NULL COMMENT 'City name',
  `galeria_cats_map` text COMMENT 'City map',
  PRIMARY KEY  (`galeria_cats_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `cms_galeria_cats`
--

INSERT INTO `cms_galeria_cats` (`galeria_cats_id`, `galeria_cats_name`, `galeria_cats_map`) VALUES
(1, 'PROYECTOS ARQUITECTÓNICOS', ''),
(2, 'MATERIALES', NULL),
(3, 'CLIENTES', NULL),
(4, 'NOSOTROS', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_galeria_config`
--

CREATE TABLE IF NOT EXISTS `cms_galeria_config` (
  `galeria_config_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `galeria_config_funcionality` int(1) NOT NULL COMMENT 'Funcionalidad del módulo',
  `galeria_config_date` datetime default NULL COMMENT 'Fecha y hora de instalación módulo',
  PRIMARY KEY  (`galeria_config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_home`
--

CREATE TABLE IF NOT EXISTS `cms_home` (
  `home_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `home_title` varchar(80) default NULL COMMENT 'home title',
  `home_description` varchar(250) default NULL COMMENT 'home description',
  `home_value` text COMMENT 'field value',
  `home_type` varchar(20) default NULL COMMENT 'home type',
  PRIMARY KEY  (`home_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `cms_home`
--

INSERT INTO `cms_home` (`home_id`, `home_title`, `home_description`, `home_value`, `home_type`) VALUES
(2, 'Destacado Título', 'título', 'Curso Aluminio para la Línea Universal', 'INPUT'),
(3, 'Destacado Cuerpo', 'Máximo 200 carácteres', 'Estamos comprometidos con la formación profesional de nuestro país en alianza con el SENA.', 'TEXTAREA'),
(4, 'Destacado URL', 'http://', 'saldefrutas.com/Vitral/beneficios.php', 'INPUT'),
(5, 'Destacado Imagen', '295 x 120 px', 'F6PG.Destacado..Senadef.jpg', 'IMAGE'),
(6, 'Video', 'Video de YouTube. Introducir código <b>V</b> del video.<br />Ejemplo: Si el video es: http://www.youtube.com/watch?v=TF6cnLnEARo introducir solo <b>TF6cnLnEARo</b>', 'v7dq4G07QaE', 'VIDEOYT');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_home_config`
--

CREATE TABLE IF NOT EXISTS `cms_home_config` (
  `home_config_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `home_config_funcionality` int(1) NOT NULL COMMENT 'Permitir añadir mas campos dinamicos',
  `home_config_date` datetime default NULL COMMENT 'Fecha y hora de instalación módulo',
  PRIMARY KEY  (`home_config_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `cms_home_config`
--

INSERT INTO `cms_home_config` (`home_config_id`, `home_config_funcionality`, `home_config_date`) VALUES
(1, 0, '2012-09-27 06:57:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_links`
--

CREATE TABLE IF NOT EXISTS `cms_links` (
  `links_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `links_title` varchar(120) default NULL COMMENT 'Link title',
  `links_url` varchar(120) default NULL COMMENT 'Link URL',
  `links_blank` tinyint(1) default '0' COMMENT 'Open in new blank page',
  `links_order` int(5) unsigned default '0' COMMENT 'Link order',
  `links_image` varchar(120) default NULL COMMENT 'Link image',
  `links_description` text COMMENT 'Link description',
  PRIMARY KEY  (`links_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `cms_links`
--

INSERT INTO `cms_links` (`links_id`, `links_title`, `links_url`, `links_blank`, `links_order`, `links_image`, `links_description`) VALUES
(1, 'Cuadernillo ASTRAL 1.6', 'I1B5.Informe Económico semestre I-2012.pdf', 0, 1, 'HTKE.Astral.1.6def.jpg', ''),
(2, 'Cuadernillo ASTRAL 1.8', 'Astral_1.8.pdf', 0, 2, 'RPWU.bg2.jpg', ''),
(3, 'Cuadernillo ASTRAL 2.0', 'I8CQ.alphaBgBlack.png', 0, 3, 'ZUI3.', ''),
(4, 'Cuadernillo COLOSAL 2.6', 'M4CH.buscarBtnFiltro.png', 0, 4, '5EC.', ''),
(6, 'Cuadernillo ASTRAL 1.7', 'Astral_1.7.pdf', 0, 2, '3YG1.', ''),
(7, 'Cuadernillo COLOSAL 345', 'WK7N.Una_Guia_para_un_Telemarketing_Exitoso.pdf', 0, 1, 'DI75.', ''),
(8, 'Cuadernillo BOREAL 1.8 PLUS', 'PWX7.Bol_ieac_Itrim12.pdf', 0, 1, 'PIY.', ''),
(9, 'Cuadernillo SIDERAL 2.4', '5G3Z.Atencion Telefonica.pdf', 0, 1, 'TJ1.', ''),
(10, 'Catalogo General VITRAL', 'Catalogo_general_Vitral_2011.pdf', 0, 1, 'B93Z.', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_links_config`
--

CREATE TABLE IF NOT EXISTS `cms_links_config` (
  `links_config_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `links_config_funcionality` int(1) NOT NULL COMMENT 'Funcionalidad del módulo',
  `links_config_date` datetime default NULL COMMENT 'Fecha y hora de instalación módulo',
  PRIMARY KEY  (`links_config_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `cms_links_config`
--

INSERT INTO `cms_links_config` (`links_config_id`, `links_config_funcionality`, `links_config_date`) VALUES
(1, 2, '2012-09-27 09:51:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_menu`
--

CREATE TABLE IF NOT EXISTS `cms_menu` (
  `menu_id` int(11) NOT NULL auto_increment,
  `menu_title` varchar(40) default NULL,
  `menu_url` varchar(80) default NULL,
  `menu_icon` varchar(20) default NULL,
  PRIMARY KEY  (`menu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `cms_menu`
--

INSERT INTO `cms_menu` (`menu_id`, `menu_title`, `menu_url`, `menu_icon`) VALUES
(1, 'Banners', 'modules/banners/view', 'pictures_folder'),
(2, 'HOME<br />Destacado y Video', 'modules/home/view', 'clipboard'),
(3, 'Encuestas', 'modules/encuesta/view', 'checkmark'),
(4, 'Cadena de Valor', 'modules/clientes/view', 'usb'),
(5, 'Nosotros', 'modules/nosotros/view', 'clipboard'),
(6, 'Descargas', 'modules/descargas/view', 'satellite'),
(7, 'Beneficios y Noticias', 'modules/noticias/view', 'calendar'),
(8, 'Galería de fotos', 'modules/galeria/view', 'pictures_folder'),
(9, 'Productos', 'modules/products/view', 'briefcase'),
(10, 'Alma', 'modules/alma/view', 'clipboard'),
(11, 'ALMA productos', 'modules/almanews/view', 'calendar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_nosotros`
--

CREATE TABLE IF NOT EXISTS `cms_nosotros` (
  `nosotros_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `nosotros_title` varchar(80) default NULL COMMENT 'nosotros title',
  `nosotros_description` varchar(120) default NULL COMMENT 'nosotros description',
  `nosotros_value` text COMMENT 'field value',
  `nosotros_type` varchar(20) default NULL COMMENT 'nosotros type',
  PRIMARY KEY  (`nosotros_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Volcado de datos para la tabla `cms_nosotros`
--

INSERT INTO `cms_nosotros` (`nosotros_id`, `nosotros_title`, `nosotros_description`, `nosotros_value`, `nosotros_type`) VALUES
(1, 'Nosotros', 'Máximo 500 carácteres', 'Somos una empresa  colombiana de carácter limitada fundada en 1.980 dedicada a la comercialización de productos de aluminio con especialidad en perfiles de uso arquitectónico. Ofrecemos asesorías y diseños de productos como fachadas, cubiertas, escenarios deportivos, estructuras espaciales y soluciones estructurales específicas.Nuestra empresa hace parte de la Corporación Empresarial S.A. CORPESA, un grupo de empresas relacionadas que actúan principalmente en el sector de la construcción, con liderazgo en el Ecuador y proyecciones hacia el mercado andino. Contamos con oficinas comerciales y bodegas de almacenamiento propias en Bogotá y Cali, dando cobertura y generando confianza a nuestro mercado. Hemos creado importantes relaciones en el canal de distribución, garantizando nuestros productos en el mercado.', 'TEXTAREA'),
(2, 'Valores', 'Máximo 500 carácteres', 'Valores\r\n•	Creemos que lograr una mejor calidad de vida es un derecho innato de todas las personas\r\n•	Creemos que las relaciones interpersonales deben desarrollarse en el respeto mutuo y con honestidad en todas las actividades\r\n•	Creemos que la búsqueda de la excelencia es prioritaria e indelegable en el desarrollo integral de la compañía\r\n•	Creemos en un compromiso proactivo con total vocación de servicio y de largo plazo\r\n•	Creemos que la empresa es el medio idóneo para lograr el desarrollo de estos valores\r\n•	Creemos que el trabajo en equipo y la innovación son la base del crecimiento y desarrollo de la organización', 'TEXTAREA'),
(3, 'Misión y Visión', 'Máximo 500 carácteres', 'MISIÓN- Somos una organización con una sólida presencia en la comercialización de perfiles de aluminio, sus accesorios y elementos complementarios, para los sectores arquitectónico e industrial a través de productos novedosos, con asesoría permanente a los clientes y usuarios como factores diferenciadores. VISIÓN - Queremos ser una organización líder en innovación de productos relacionados con perfilería de aluminio, competitiva en mercados globalizados, reconocida por la excelencia de sus servicios, la calidad de sus productos y la rentabilidad de su gestión.', 'TEXTAREA'),
(4, 'Propuesta de Valor', 'Máximo 500 carácteres', 'Somos el Respaldo de su Venta', 'TEXTAREA'),
(5, 'Política de Calidad', 'Máximo 500 carácteres', 'El principio básico de nuestra Política de calidad se basa en el conocimiento cercano y permanente de las  necesidades de cada cliente.\r\nEn Vitral, reconocemos nuestros clientes deben utilizar, productos con cero defectos por fabricación, funcionalidad y diseño. Garantizando así la vida útil de cualquier producto elaborado por un término de cinco años calendario con reposición inmediata del producto cuando la ocurrencia haya sido comprobada de acuerdo con el sistema de gestión de la empresa.', 'TEXTAREA'),
(6, 'Política de Venta', 'Máximo 500 carácteres', 'En Vitral, atendemos a nuestros clientes con un sentido de servicio, claro, serio, responsable y profesional, a través de la cadena productiva y defendiendo la libre competencia.', 'TEXTAREA'),
(7, 'Materia Prima', 'Máximo 500 carácteres', 'Utilizamos aleaciones primarias con garantía de pureza y composición. \r\n\r\nLas aleaciones secundarias son elaboradas en hornos de fundición con estrictos procesos de suministro de materias primas, componentes de las aleaciones y estabilización de productos. \r\n', 'TEXTAREA'),
(8, 'Presentación', 'Máximo 500 carácteres', '•	En paquetes de perfiles que cumplan con una relación básica entre peso, tamaño y cantidad. \r\n•	Su tamaño promedio de 20.5 X 20.5 x 6 metros de longitud, con un peso aproximado de 30 kilos por unidad, facilitan su manipulación, ubicación y almacenamiento. \r\n•	Son paquetes prácticos de contar y sencillos de valorizar\r\n', 'TEXTAREA'),
(9, 'Acabados', 'Máximo 500 carácteres', '•	El anodizado es un proceso electrolítico de oxidación controlada. \r\n•	Los tonos se producen por la utilización de sales minerales, energía eléctrica y tiempos de inmersión en los tanques, que garantizan los espesores de la capa anódica desde 10 hasta 24 micras. \r\n•	La calidad del anodizado depende de un buen sellado que determina la estabilidad del tono original. \r\n•	El sellado se debe efectuar en agua desmineralizada a una temperatura próxima a los 100C o con procesos químicos de sellado en frío. \r\n•	La pintura al horno se genera en un proceso electroestático. Su calidad se determina por el tipo de pintura que se utilice. En los productos VITRAL se aplican los de tipo Poliéster y Uretano cuyos colores están identificados con la sigla RAL, garantizados por 5 años en condiciones ambientales normales.\r\n', 'TEXTAREA'),
(10, 'Calidad', 'Máximo 500 carácteres', '•	Paquetes de colores que facilitan su identificación\r\n•	Tamaños y cantidades permanentes y confiables.\r\n•	Variedad de diseños e inventarios. \r\n•	Equivalencias en sistemas arquitectónicos. La cantidad de perfiles de la unidad de empaque está determinada, por la ubicación y el uso de cada referencia en los diferentes diseños de divisiones, puertas, ventanas y fachadas. \r\n•	La marca Vitral de fábrica, se reconoce por una línea en alto relieve situada en una de las caras no expuestas en el perfil. \r\n•	La marca Vitral respalda la garantía por defectos de fabricación, aleación o \r\nacabado de su perfilería.', 'TEXTAREA'),
(11, 'Imagen 1', '[438 x 230px]', 'CB2K.Quienes somosdef.jpg', 'IMAGE'),
(12, 'Imagen 2', '[438 x 230px]', 'U2PG.FOT_0097def.jpg', 'IMAGE'),
(13, 'Imagen 3', '[438 x 230px]', '3TJP.Quienes.somos4def.jpg', 'IMAGE'),
(14, 'proceso de fabricacion', 'Máximo 500 carácteres', '•	Maquinado a presión a través de un molde o matriz, previo calentamiento a altas temperaturas\r\n•	Por la planimetría se verifican torsiones o dobladuras del perfil\r\n•	El temple nos proporciona una dureza uniforme del perfil, logrado en el horno de envejecimiento\r\n•	La dureza se mide en grados webster de 10 a 12 unidades\r\n•	La aleación garantiza las propiedades mecánicas del perfil\r\n•	A mayor grosor mayor peso. Esto nos facilita un mejor maquinado para doblar, cortar, perforar y es garantía de estabilidad en el tiempo', 'TEXTAREA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_nosotros_config`
--

CREATE TABLE IF NOT EXISTS `cms_nosotros_config` (
  `nosotros_config_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `nosotros_config_funcionality` int(1) NOT NULL COMMENT 'Permitir añadir mas campos dinamicos',
  `nosotros_config_date` datetime default NULL COMMENT 'Fecha y hora de instalación módulo',
  PRIMARY KEY  (`nosotros_config_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `cms_nosotros_config`
--

INSERT INTO `cms_nosotros_config` (`nosotros_config_id`, `nosotros_config_funcionality`, `nosotros_config_date`) VALUES
(1, 0, '2012-09-27 09:38:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_noticias`
--

CREATE TABLE IF NOT EXISTS `cms_noticias` (
  `noticias_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `noticias_title` varchar(80) default NULL COMMENT 'NOTICIA title',
  `noticias_date` int(11) unsigned default '0' COMMENT 'NOTICIA fecha timestamp',
  `noticias_img1` varchar(120) default NULL COMMENT 'IMAGEN 1',
  `noticias_img2` varchar(100) default NULL COMMENT 'IMAGEN 1',
  `noticias_order` int(5) unsigned default '0' COMMENT 'NOTICIA order',
  `noticias_description` text COMMENT 'NOTICIA descripcion',
  `noticias_shortdescription` text COMMENT 'NOTICIA descripcion corta',
  `noticias_lang` varchar(100) default 'es' COMMENT 'Idioma',
  PRIMARY KEY  (`noticias_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `cms_noticias`
--

INSERT INTO `cms_noticias` (`noticias_id`, `noticias_title`, `noticias_date`, `noticias_img1`, `noticias_img2`, `noticias_order`, `noticias_description`, `noticias_shortdescription`, `noticias_lang`) VALUES
(1, 'Comprometidos con la educación', 1348642800, 'JP4G.sena def.jpg', '', 1, 'El Sena y Vitral Ltda para el año 2012, ha desarrollado un programa de educación certificada en “Aluminio para la Línea Universal” donde se busca fortalecer los conocimientos del aluminio, especializado en ventanas y puertas arquitectónicas. Felicitaciones a los más de 90 graduados.', '', 'Vitral Ltda'),
(2, 'Logro sobresaliente', 1348642800, 'DYAB.Logro def.jpg', '', 1, 'ALCOA, líder mundial en la producción de aluminio primario y revestimiento de paneles compuestos, otorgo un reconocimiento a ESTRUSA por su desempeño en la utilización y venta de paneles en el año 2010. Nuestras felicitaciones. Quito – Ecuador.', '', 'Cedal'),
(4, 'Un techo para Colombia', 1299657600, '3D7J.Techo def.jpg', '', 1, 'Una actividad de responsabilidad social patrocinada por la Gerencia General. Fue una experiencia maravillosa, donde una familia integrada por 10 personas, recibió una casa bajo el programa “Un techo para Colombia”. Bogotá - Colombia', '', 'Vitral Ltda'),
(5, 'Cedal premia sus clientes', 1298966400, 'ZYVI.Clientes def.jpg', '', 1, 'Se realizó el sorteo extraordinario de la tarjeta premia Cedal. Felicitaciones a los ganadores. Quito – Ecuador', '', 'Vitral Ltda');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_noticias_config`
--

CREATE TABLE IF NOT EXISTS `cms_noticias_config` (
  `noticias_config_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `noticias_config_funcionality` int(1) NOT NULL COMMENT 'Funcionalidad del módulo',
  `noticias_config_date` datetime default NULL COMMENT 'Fecha y hora de instalación módulo',
  PRIMARY KEY  (`noticias_config_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `cms_noticias_config`
--

INSERT INTO `cms_noticias_config` (`noticias_config_id`, `noticias_config_funcionality`, `noticias_config_date`) VALUES
(1, 0, '2012-09-27 10:00:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_products`
--

CREATE TABLE IF NOT EXISTS `cms_products` (
  `products_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `products_id_cat` int(11) unsigned NOT NULL COMMENT 'id cat',
  `products_id_subcat` int(11) unsigned NOT NULL COMMENT 'id sub-cat',
  `products_title` varchar(100) default NULL COMMENT 'Product title',
  `products_price` varchar(80) default NULL COMMENT 'Product price',
  `products_ref` varchar(80) default NULL COMMENT 'Product ref',
  `products_size` varchar(80) default NULL COMMENT 'Product size',
  `products_stared` tinyint(1) default '0' COMMENT 'Product stared',
  `products_description` varchar(200) default '0' COMMENT 'Product stared',
  `products_image1` varchar(80) default NULL COMMENT 'Product main image',
  `products_image2` varchar(80) default NULL COMMENT 'Product image',
  `products_image3` varchar(80) default NULL COMMENT 'Product image',
  `products_image4` varchar(80) default NULL COMMENT 'Product image',
  `products_color_image1` varchar(80) default NULL COMMENT 'Product color image',
  `products_color_image2` varchar(80) default NULL COMMENT 'Product color image',
  `products_color_image3` varchar(80) default NULL COMMENT 'Product color image',
  `products_color_image4` varchar(80) default NULL COMMENT 'Product color image',
  `products_color_image5` varchar(80) default NULL COMMENT 'Product color image',
  PRIMARY KEY  (`products_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=479 ;

--
-- Volcado de datos para la tabla `cms_products`
--

INSERT INTO `cms_products` (`products_id`, `products_id_cat`, `products_id_subcat`, `products_title`, `products_price`, `products_ref`, `products_size`, `products_stared`, `products_description`, `products_image1`, `products_image2`, `products_image3`, `products_image4`, `products_color_image1`, `products_color_image2`, `products_color_image3`, `products_color_image4`, `products_color_image5`) VALUES
(1, 2, 2, 'Alfajía', '', '2169', '', 0, 'Alfajías', 'SP9.2169 alfajia de 16 cm.jpg', '', '', '', '', '', '', '', ''),
(2, 2, 2, 'Alfajía 114 mm', '', '2544', '', 0, 'Alfajías', 'E943.2544 alfajia de 11.5 cm.jpg', '', '', '', '', '', '', '', ''),
(4, 2, 2, 'Alfajía 8 mm', '', '1743', '', 0, 'Alfajía', 'MGN.1743 alfajia de 8 cm.jpg', '', '', '', '', '', '', '', ''),
(8, 2, 15, 'Ángulo de1  21/32"  x  31/64"', '', '1179', '', 0, 'Ángulo', '4TPM.angulos de aletas desiguales.jpg', '', '', '', '', '', '', '', ''),
(9, 2, 15, 'Ángulo de 1  31/64"  X 1  17/32"', '', '1248', '', 0, 'Ángulo', '1JLM.angulos de aletas desiguales.jpg', '', '', '', '', '', '', '', ''),
(10, 2, 15, 'Ángulo de 3  15/16"  X 3/4"', '', '1319', '', 0, 'Ángulo', 'CZLC.angulos de aletas desiguales.jpg', '', '', '', '', '', '', '', ''),
(11, 2, 15, 'Ángulo de  2  3/4"  X 3/4"', '', '1422', '', 0, 'Ángulo', '6X6G.angulos de aletas desiguales.jpg', '', '', '', '', '', '', '', ''),
(12, 2, 15, 'Ángulo de 1  5/16"  X 3/4"', '', '1512', '', 0, 'Ángulos', 'SSQ.angulos de aletas desiguales.jpg', '', '', '', '', '', '', '', ''),
(13, 2, 15, 'Ángulo de 2 3/64" x 3/4" ', '', '2263', '', 0, 'Ángulos', '9ENV.angulos de aletas desiguales.jpg', '', '', '', '', '', '', '', ''),
(14, 2, 15, 'Ángulo de 1 x 1" x 1/8" ', '', '1715', '', 0, 'Ángulos', '1LGC.angulos de aletas iguales.jpg', '', '', '', '', '', '', '', ''),
(15, 2, 15, 'Ángulo', '', '1921', '', 0, '1" de puntas redondas ', '8E7C.angulos de aletas iguales.jpg', '', '', '', '', '', '', '', ''),
(16, 2, 15, 'Ángulo de 1" x 1" x 1/16" ', '', '1182', '', 0, 'Ángulos', '9UXY.angulos de aletas iguales.jpg', '', '', '', '', '', '', '', ''),
(17, 2, 15, 'Ángulo de 1  1/2" x 1 1/2" x 1/8" ', '', '2029', '', 0, 'Ángulos', 'D2E.angulos de aletas iguales.jpg', '', '', '', '', '', '', '', ''),
(19, 2, 15, 'Ángulo de 1 1/4', '', '2033', '', 0, 'Ángulos', 'T4IL.angulos de aletas iguales.jpg', '', '', '', '', '', '', '', ''),
(20, 2, 15, 'Ángulo de 11.5 mm', '', '2407', '', 0, 'Ángulos', '7IVU.angulos de aletas iguales.jpg', '', '', '', '', '', '', '', ''),
(21, 2, 15, 'Ángulo de 17 mm x 17 mm x 0.040 mm', '', '1993', '', 0, 'Ángulos', 'B3C6.angulos de aletas iguales.jpg', '', '', '', '', '', '', '', ''),
(22, 2, 15, 'Ángulo', '', '1236', '', 0, '19 x 19 mm', 'JV1T.angulos de aletas iguales.jpg', '', '', '', '', '', '', '', ''),
(23, 2, 15, 'Ángulo', '', '1639', '', 0, '2" x 3/1', 'JINH.angulos de aletas iguales.jpg', '', '', '', '', '', '', '', ''),
(24, 2, 15, 'Ángulo', '', '1639', '', 0, '2\\" x 3/1', 'BRJV.angulos de aletas iguales.jpg', '', '', '', '', '', '', '', ''),
(25, 2, 15, 'Ángulo', '', '2835', '', 0, '3/4 x 3/4 x 1/8', 'JS9M.angulos de aletas iguales.jpg', '', '', '', '', '', '', '', ''),
(26, 2, 15, 'Ángulo de fijación proyectante', '', '1449', '', 0, '', 'SPKU.angulos de aletas iguales.jpg', '', '', '', '', '', '', '', ''),
(27, 2, 15, 'Ángulo', '', '1444', '', 0, '1 1/4\\"', 'KZH.angulos de aletas iguales.jpg', '', '', '', '', '', '', '', ''),
(28, 2, 15, 'Ángulo', '', '1554', '', 0, '1  7/64\\"', 'C222.angulos de aletas iguales.jpg', '', '', '', '', '', '', '', ''),
(29, 2, 15, 'Ángulo', '', '1638', '', 0, '1  61/32\\"', 'FUY3.angulos de aletas iguales.jpg', '', '', '', '', '', '', '', ''),
(30, 2, 15, 'Ángulo', '', '1640', '', 0, '1  61/32\\"', '2JK6.angulos de aletas iguales.jpg', '', '', '', '', '', '', '', ''),
(31, 2, 15, 'Ángulo', '', '1779', '', 0, '3/4\\"', 'YCDE.angulos de aletas iguales.jpg', '', '', '', '', '', '', '', ''),
(32, 2, 5, 'Marco ranurado de 85 mm ', '', '2375', '', 0, 'Avisos publicitarios', '3IV8.2375 marco ranurado de 85 x 15.3.jpg', '', '', '', '', '', '', '', ''),
(33, 2, 5, 'Marco con U invertida', '', '2377', '', 0, 'Avisos publicitarios', 'KKGH.2377 marco con U invertida.jpg', '', '', '', '', '', '', '', ''),
(34, 2, 5, 'Marco ranurado de 75.4 mm ', '', '2376', '', 0, 'Avisos publicitarios', '5ZLH.2376 marco ranurado.jpg', '', '', '', '', '', '', '', ''),
(35, 2, 5, 'Marco ranurado de 100 mm', '', '2784', '', 0, 'Avisos publicitarios', 'TBSQ.2784 marco ranurado de 100.jpg', '', '', '', '', '', '', '', ''),
(36, 2, 61, 'F para vidrio de seguridad', '', '3013', '', 0, '', 'SAX.3013 F de seguridad.jpg', '', '', '', '', '', '', '', ''),
(37, 2, 61, 'Zócalo', '', '2282', '', 0, '100 mm', 'Y9LT.2282 Zocalo de 10 cm.jpg', '', '', '', '', '', '', '', ''),
(38, 2, 61, 'Zócalo 90 mm', '', '1816', '', 0, 'Zócalos', 'Y4TP.1816 Zocalo de 9 cm.jpg', '', '', '', '', '', '', '', ''),
(40, 2, 60, 'Divisor', '', '2811', '', 0, '', 'R43G.2811 Divisor.jpg', '', '', '', '', '', '', '', ''),
(41, 2, 60, 'Marco autorroscante', '', '2930', '', 0, '', 'UBB7.2930 Marco autoroscante.jpg', '', '', '', '', '', '', '', ''),
(42, 2, 60, 'Cabezal sillar', '', '2929', '', 0, '', 'D2YY.2929 Sillar cabezal.jpg', '', '', '', '', '', '', '', ''),
(43, 2, 60, 'Marco nave', '', '3229', '', 0, '', 'BSII.3229 Marco Batiente.jpg', '', '', '', '', '', '', '', ''),
(44, 2, 60, 'Pisavidrio curvo', '', '2966', '', 0, '', 'NAAR.2966 Pisavidrio curvo.jpg', '', '', '', '', '', '', '', ''),
(45, 2, 60, 'Pisavidrio doble', '', '2967', '', 0, '', 'IEXZ.2967 Pisavidrio doble vidrio.jpg', '', '', '', '', '', '', '', ''),
(46, 2, 60, 'Pisavidrio', '', '2999', '', 0, '', '5Z9E.2999 Pisavidrio 10 mm.jpg', '', '', '', '', '', '', '', ''),
(47, 2, 60, 'Pisavidrio', '', '2813', '', 0, '', 'AFAM.2813 Pisavidrio.jpg', '', '', '', '', '', '', '', ''),
(48, 2, 60, 'Remate esquinero', '', '3230', '', 0, '', 'AQB6.3230 Esquinero.jpg', '', '', '', '', '', '', '', ''),
(49, 2, 60, 'T divisora', '', '3228', '', 0, '', 'PXIL.3228 Divisor.jpg', '', '', '', '', '', '', '', ''),
(50, 2, 59, 'Entrecierre', '', '2324', '', 0, '', 'WZV3.2324 Enganche.jpg', '', '', '', '', '', '', '', ''),
(51, 2, 59, 'Horizontal inferior de nave', '', '2327', '', 0, '', 'QKK6.2327 Htal inf nave.jpg', '', '', '', '', '', '', '', ''),
(52, 2, 59, 'Horizontal superior con felpero', '', '2326', '', 0, '', 'SUSL.2326 Htal sup con felpero.jpg', '', '', '', '', '', '', '', ''),
(53, 2, 59, 'Cabezal Jamba', '', '2322', '', 0, '', 'PB2H.2322 Cabezal jamba.jpg', '', '', '', '', '', '', '', ''),
(54, 2, 59, 'Sillar alfajía', '', '2410', '', 0, '', 'KBQG.2410 Sillar Alfajia.jpg', '', '', '', '', '', '', '', ''),
(55, 2, 59, 'Sillar', '', '2323', '', 0, '', 'R7CJ.2323 Sillar.jpg', '', '', '', '', '', '', '', ''),
(56, 2, 59, 'Cabezal', '', '2330', '', 0, '', 'RSJB.2330 Cabezal.jpg', '', '', '', '', '', '', '', ''),
(57, 2, 59, 'Vertical nave', '', '2325', '', 0, '', 'V1X6.2325 Vertical nave.jpg', '', '', '', '', '', '', '', ''),
(58, 2, 58, 'Cabezal doble', '', '2611', '', 0, '', '7T8B.2611 Cabezal doble.jpg', '', '', '', '', '', '', '', ''),
(59, 2, 58, 'Cabezal un riel', '', '2603', '', 0, '', 'Y7V9.2603 Cabezal un riel.jpg', '', '', '', '', '', '', '', ''),
(60, 2, 58, 'Enganche', '', '2626', '', 0, '', 'EWGP.2626 Enganche.jpg', '', '', '', '', '', '', '', ''),
(61, 2, 58, 'Horizontal nave fija', '', '2608', '', 0, '', 'QF4Y.2608 Horizontal nave fija.jpg', '', '', '', '', '', '', '', ''),
(62, 2, 58, 'Jamba fija', '', '2624', '', 0, '', 'R2ZI.2624 jamba fija.jpg', '', '', '', '', '', '', '', ''),
(63, 2, 58, 'Jamba movil', '', '2623', '', 0, '', 'V5TR.2623 Jamba movíl.jpg', '', '', '', '', '', '', '', ''),
(64, 2, 58, 'Sillar doble', '', '2612', '', 0, '', 'SQLE.2612 Sillar doble.jpg', '', '', '', '', '', '', '', ''),
(65, 2, 58, 'Sillar un riel', '', '2604', '', 0, '', 'RXJ.2604 Sillar un riel.jpg', '', '', '', '', '', '', '', ''),
(66, 2, 58, 'Traslape', '', '2610', '', 0, '', 'FF8.2610 Traslape.jpg', '', '', '', '', '', '', '', ''),
(67, 2, 58, 'Platina de fijación', '', '2614', '', 0, '', 'RIR7.2614 Platina de fijación.jpg', '', '', '', '', '', '', '', ''),
(68, 2, 57, 'Cabezal', '', '2830', '', 0, '', '92H4.2830 Cabezal.jpg', '', '', '', '', '', '', '', ''),
(69, 2, 57, 'Enganche', '', '1863', '', 0, '', 'T8FT.1863 Enganche.jpg', '', '', '', '', '', '', '', ''),
(70, 2, 57, 'Enganche sin pestaña', '', '2834', '', 0, '', 'JY3S.2834 Enganche.jpg', '', '', '', '', '', '', '', ''),
(71, 2, 57, 'Horizontal superior e inferior', '', '2833', '', 0, '', 'MN7Y.2833 horizontal sup e inf.jpg', '', '', '', '', '', '', '', ''),
(72, 2, 57, 'Jamba', '', '2831', '', 0, '', 'JXVL.2831 Jamba.jpg', '', '', '', '', '', '', '', ''),
(73, 2, 57, 'Sillar alfajía', '', '2413', '', 0, '', '5I5M.2413 Sillar Alfajia.jpg', '', '', '', '', '', '', '', ''),
(74, 2, 57, 'Sillar', '', '2829', '', 0, '', 'LQXX.2829 Sillar.jpg', '', '', '', '', '', '', '', ''),
(75, 2, 57, 'Traslape para cerradura', '', '1862', '', 0, '', '6BWW.1862 Traslape para cerradura.jpg', '', '', '', '', '', '', '', ''),
(76, 2, 57, 'Traslape', '', '2832', '', 0, '', 'BTT.2832 Traslape.jpg', '', '', '', '', '', '', '', ''),
(77, 1, 14, 'Adaptador', '', '3197', '', 0, 'Ventana Corrediza Astral 1.8', 'IQQD.3197 Adaptador.jpg', '', '', '', '', '', '', '', ''),
(78, 1, 14, 'Enganche', '', '3380', '', 0, '', 'AHP.3380 Entrecierre.jpg', '', '', '', '', '', '', '', ''),
(79, 1, 14, 'Entrecierre', '', '3195', '', 0, '', '7RI5.3195 Enganche.jpg', '', '', '', '', '', '', '', ''),
(80, 1, 14, 'Horizontal inferior', '', '3404', '', 0, '', 'INS.3404 Horizontal Inferior.jpg', '', '', '', '', '', '', '', ''),
(81, 1, 14, 'Horizontal inferior nave', '', '3193', '', 0, '', '7PQ9.3193 Horizontal inferior.jpg', '', '', '', '', '', '', '', ''),
(82, 1, 14, 'Jamba marco', '', '3191', '', 0, '', 'DHQ1.3191 Jamba marco.jpg', '', '', '', '', '', '', '', ''),
(83, 1, 14, 'Vertical de cerradura', '', '3194', '', 0, '', 'MZPW.3194 Vertical de cerradura.jpg', '', '', '', '', '', '', '', ''),
(84, 1, 13, 'Anjeo perimetral', '', '3196', '', 0, '', 'FLA8.3196 Anjeo perimetral.jpg', '', '', '', '', '', '', '', ''),
(85, 1, 13, 'Divisor panoramico', '', '3198', '', 0, '', 'ZSTF.3198 Divisor panoramico.jpg', '', '', '', '', '', '', '', ''),
(86, 1, 13, 'Divisor de anjeo', '', '3218', '', 0, '', 'D3K4.3218 Divisor anjeo.jpg', '', '', '', '', '', '', '', ''),
(87, 1, 13, 'T divisora cuerpo fijo', '', '3436', '', 0, '', 'ZY7W.3436 T divisora cuerpo fijo.jpg', '', '', '', '', '', '', '', ''),
(88, 1, 13, 'Entrecierre', '', '3380', '', 0, '', 'RBTT.3380 Entrecierre.jpg', '', '', '', '', '', '', '', ''),
(89, 1, 13, 'Horizontal inferior', '', '3404', '', 0, '', 'XSBF.3404 Horizontal Inferior.jpg', '', '', '', '', '', '', '', ''),
(90, 1, 13, 'Jamba marco tres pistas', '', '3201', '', 0, '', '9GVM.3201 jamba marco tres pistas.jpg', '', '', '', '', '', '', '', ''),
(91, 1, 13, 'Jamba cuerpo fijo', '', '3177', '', 0, '', 'JDQ8.3177 Jamba de cuerpo fijo.jpg', '', '', '', '', '', '', '', ''),
(92, 1, 13, 'Jamba marco', '', '3191', '', 0, '', 'DVVE.3191 Jamba marco.jpg', '', '', '', '', '', '', '', ''),
(93, 1, 13, 'Riel inferior de anjeo', '', '3205', '', 0, '', 'XJG.3205 Riel inferior de anjeo.jpg', '', '', '', '', '', '', '', ''),
(94, 1, 13, 'Riel infeior de marco', '', '3200', '', 0, '', 'JN3S.3200 Riel inferior de marco.jpg', '', '', '', '', '', '', '', ''),
(95, 1, 13, 'Riel superior de anjeo', '', '3204', '', 0, '', '2L6.3204 Riel superior de anjeo.jpg', '', '', '', '', '', '', '', ''),
(96, 1, 13, 'Riel superior', '', '3199', '', 0, '', 'UF4N.3199 Riel superior.jpg', '', '', '', '', '', '', '', ''),
(97, 1, 13, 'Horizontal superior', '', '3202', '', 0, '', '7P9Z.3202 Horizontal superior.jpg', '', '', '', '', '', '', '', ''),
(98, 1, 13, 'Vertical de cerradura', '', '3194', '', 0, '', 'GMS.3194 Vertical de cerradura.jpg', '', '', '', '', '', '', '', ''),
(99, 1, 12, 'Adaptador', '', '3187', '', 0, 'Ventana corrediza Astral 1.6', 'RWZ.3187 Adaptador sencillo.jpg', '', '', '', '', '', '', '', ''),
(100, 1, 12, 'Divisor panoramico', '', '3188', '', 0, '', 'F2UU.3188 Divisor panoramico.jpg', '', '', '', '', '', '', '', ''),
(101, 1, 12, 'T divisora cuerpo fijo', '', '3436', '', 0, '', 'CFDZ.3436 T divisora cuerpo fijo.jpg', '', '', '', '', '', '', '', ''),
(102, 1, 12, 'Enganche cuerpo fijo', '', '3435', '', 0, '', 'Y1HW.3435 Enganche cuerpo fijo.jpg', '', '', '', '', '', '', '', ''),
(103, 1, 12, 'Entrecierre fijo', '', '3183', '', 0, '', 'QP7X.3183 entrecierre fijo.jpg', '', '', '', '', '', '', '', ''),
(104, 1, 12, 'Entrecierre de nave', '', '3182', '', 0, '', 'JD69.3182 Entrecierre de nave.jpg', '', '', '', '', '', '', '', ''),
(105, 1, 12, 'Horizontal inferior de nave', '', '3179', '', 0, '', 'UCVN.3179 Horizontal inferior de nave.jpg', '', '', '', '', '', '', '', ''),
(106, 1, 12, 'Jamba cuerpo fijo', '', '3177', '', 0, '', '8XIY.3177 Jamba de cuerpo fijo.jpg', '', '', '', '', '', '', '', ''),
(107, 1, 12, 'Jamba cuerpo movil', '', '3176', '', 0, '', 'RKNH.3176 Jamba Cuerpo Movil.jpg', '', '', '', '', '', '', '', ''),
(108, 1, 12, 'Refuerzo vertical', '', '3186', '', 0, '', 'F9XA.3186 Refuerzo vertical.jpg', '', '', '', '', '', '', '', ''),
(109, 1, 12, 'Rejilla anticondensación', '', '3189', '', 0, '', '5ZUU.3189 Rejilla anticondensación.jpg', '', '', '', '', '', '', '', ''),
(110, 1, 12, 'Sillar alfajía', '', '3185', '', 0, '', 'HRRR.3185 Riel alfajia.jpg', '', '', '', '', '', '', '', ''),
(111, 1, 12, 'Riel cuerpo fijo', '', '3180', '', 0, '', '19C7.3180 Riel cuerpo fijo.jpg', '', '', '', '', '', '', '', ''),
(112, 1, 12, 'Riel inferior de marco', '', '3175', '', 0, '', '8KMT.3175 Riel inferior de marco.jpg', '', '', '', '', '', '', '', ''),
(113, 1, 12, 'Riel superior de marco', '', '3174', '', 0, '', 'D1LL.3174 Riel superior de marco.jpg', '', '', '', '', '', '', '', ''),
(114, 1, 12, 'Horizontal de nave', '', '3178', '', 0, '', 'U6EE.3178 Horizontal de nave.jpg', '', '', '', '', '', '', '', ''),
(115, 1, 12, 'Vertical de cerradura', '', '3181', '', 0, '', 'UMRH.3181 Vertical de cerradura.jpg', '', '', '', '', '', '', '', ''),
(116, 1, 11, 'Adaptador', '', '3391', '', 0, 'Ventana batiente 1.8 Plus', '35KI.3391 Adaptador.jpg', '', '', '', '', '', '', '', ''),
(117, 1, 11, 'Divisor', '', '3390', '', 0, '', 'A3J.3390 Divisor.jpg', '', '', '', '', '', '', '', ''),
(118, 1, 11, 'Marco autorroscante', '', '2930', '', 0, '', 'EPE8.2930 Marco autoroscante.jpg', '', '', '', '', '', '', '', ''),
(119, 1, 11, 'Marco horizontal', '', '3389', '', 0, '', 'R1EZ.3389 Marco Htal.jpg', '', '', '', '', '', '', '', ''),
(120, 1, 11, 'Marco batiente', '', '3229', '', 0, '', '9IE5.3229 Marco Batiente.jpg', '', '', '', '', '', '', '', ''),
(121, 1, 11, 'Pisavidrio curvo', '', '2966', '', 0, '', '9EG2.2966 Pisavidrio curvo.jpg', '', '', '', '', '', '', '', ''),
(122, 1, 11, 'Pisavidrio', '', '2813', '', 0, '', 'ZPM.2813 Pisavidrio.jpg', '', '', '', '', '', '', '', ''),
(123, 1, 10, 'Pisavidrio doble chaflan', '', '3233', '', 0, '', 'R7QH.3233 Pisavidrio doble chaflan.jpg', '', '', '', '', '', '', '', ''),
(124, 1, 10, 'Divisor doble', '', '3227', '', 0, '', 'T8K2.3227 Doble divisor.jpg', '', '', '', '', '', '', '', ''),
(125, 1, 10, 'Marco horizontal', '', '3225', '', 0, '', 'IDZ4.3225 Marco Htal.jpg', '', '', '', '', '', '', '', ''),
(126, 1, 10, 'Marco vertical', '', '3226', '', 0, '', 'MSMW.3226 Marco Vtal.jpg', '', '', '', '', '', '', '', ''),
(127, 1, 10, 'Pisavidrio doble curvo', '', '3231', '', 0, '', 'T62M.3231 Pisavidrio doble curvo.jpg', '', '', '', '', '', '', '', ''),
(128, 1, 10, 'Pisavidrio doble recto', '', '3232', '', 0, '', 'GFUY.3232 Pisavidrio doble recto.jpg', '', '', '', '', '', '', '', ''),
(129, 1, 10, 'Pisavidrio curvo', '', '2966', '', 0, '', 'RS15.2966 Pisavidrio curvo.jpg', '', '', '', '', '', '', '', ''),
(130, 1, 10, 'Pisavidrio', '', '2813', '', 0, '', 'JJS.2813 Pisavidrio.jpg', '', '', '', '', '', '', '', ''),
(131, 2, 56, 'Paral ovalo', '', '2627', '', 0, '', 'EIMQ.2627 paral ovalo.jpg', '', '', '', '', '', '', '', ''),
(132, 2, 56, 'Perfil cocina', '', '2798', '', 0, '', 'XQPU.2798 perfil cocina.jpg', '', '', '', '', '', '', '', ''),
(133, 2, 56, 'Perfil cocina integral', '', '3252', '', 0, '', '5IFT.3252 perfil cocina.jpg', '', '', '', '', '', '', '', ''),
(134, 2, 56, 'Persiana tubular', '', '3296', '', 0, '', 'I55.3296 persiana tubular.jpg', '', '', '', '', '', '', '', ''),
(135, 2, 56, 'Soporte tapa muebles', '', '2630', '', 0, '', '8QS9.2630 soporte tapa muebles.jpg', '', '', '', '', '', '', '', ''),
(136, 2, 56, 'Tubular con adaptador', '', '2965', '', 0, '1 1/2\\" x 1 1/2\\"', 'CTW.2965 tubular 1 1 2 con adaptador.jpg', '', '', '', '', '', '', '', ''),
(137, 2, 56, 'Tubular doble aleta', '', '2087', '', 0, '1\\" x 1\\"', '4C7V.2087 1 x 1 con doble aleta.jpg', '', '', '', '', '', '', '', ''),
(138, 2, 56, 'Tubular con adaptador', '', '2964', '', 0, '3" x 1 1/2"', 'ETZW.2964 tubular con adaptador.jpg', '', '', '', '', '', '', '', ''),
(139, 2, 56, 'Tubular con doble adaptador', '', '3286', '', 0, '', 'HYQS.3286 tubular con doble adaptador.jpg', '', '', '', '', '', '', '', ''),
(140, 2, 56, 'Tubo doble con aleta', '', '1716', '', 0, '3/4\\" x 3/4\\"', 'IQ5.1716 tubo con doble aleta.jpg', '', '', '', '', '', '', '', ''),
(141, 2, 56, 'Tubular con aleta', '', '2895', '', 0, '3/4\\" x 5/8\\"', 'VZ31.2895 3 4 x 5 8 con aleta.jpg', '', '', '', '', '', '', '', ''),
(142, 2, 56, 'Tubular con aleta', '', '2037', '', 0, '1\\" x 1\\"', 'FPQ7.2037 1 x1 con aleta.jpg', '', '', '', '', '', '', '', ''),
(143, 2, 55, 'Tubular liso', '', '2857', '', 0, '2 x 1', 'IUMM.2857 Tubular Liso.jpg', '', '', '', '', '', '', '', ''),
(144, 2, 55, 'Tubular liso', '', '1507', '', 0, ' 4\\" x 1 3/4\\" ', 'N8BT.1507 Tub Liso.jpg', '', '', '', '', '', '', '', ''),
(145, 2, 55, 'Tubo con aleta', '', '2085', '', 0, '3 x 1 1/2\\"', 'RC3T.2085 Tub con aleta desc.jpg', '', '', '', '', '', '', '', ''),
(146, 2, 55, 'Tubular con aleta', '', '2903', '', 0, '2\\" x 1\\"', '1QFP.2903 Tubular con aleta desc.jpg', '', '', '', '', '', '', '', ''),
(147, 2, 55, 'Tubular con aleta', '', '2900', '', 0, '3\\" x 1\\"', 'DRYI.2900 Tub con aleta desc.jpg', '', '', '', '', '', '', '', ''),
(148, 2, 55, 'Tubo rectangular con aleta', '', '1509', '', 0, '4\\"  x 1  3/4\\"', '6DNE.1509 Tub con aleta centrada.jpg', '', '', '', '', '', '', '', ''),
(149, 2, 55, 'Tubular con aletas', '', '2086', '', 0, '3\\" x 1 1/2\\"', 'L6PB.2086 tub doble aleta desc.jpg', '', '', '', '', '', '', '', ''),
(150, 2, 55, 'Tubular con doble aleta', '', '2898', '', 0, '3! x 1\\"', 'MIQU.2898 Tub con doble aleta desc.jpg', '', '', '', '', '', '', '', ''),
(151, 2, 55, 'Tubo rectangular doble aleta', '', '1287', '', 0, '4\\"  x 1  3/4\\"', 'JMZB.1287 Tub doble aleta desc.jpg', '', '', '', '', '', '', '', ''),
(152, 2, 50, 'Peldaño circular escalera', '', '2118', '', 0, '', 'KCQI.2118 peldaño circular esc extension.jpg', '', '', '', '', '', '', '', ''),
(158, 2, 48, 'Tubular doble aleta centrada', '', '1287', '', 0, '', '7VGW.1287 Tub doble aleta desc.jpg', '', '', '', '', '', '', '', ''),
(159, 2, 48, 'Canal con aleta centrada', '', '1508', '', 0, '', 'WTB3.1508 canal con aleta desc.jpg', '', '', '', '', '', '', '', ''),
(160, 2, 48, 'Tubular con aleta centada', '', '1509', '', 0, '', 'EDI4.1509 Tub con aleta centrada.jpg', '', '', '', '', '', '', '', ''),
(161, 2, 48, 'Canal liso', '', '1510', '', 0, '4\\" x 1 3/4\\"', 'GUPR.1510 Canal Liso.jpg', '', '', '', '', '', '', '', ''),
(162, 2, 48, 'Tubular liso', '', '1507', '', 0, '4\\" x 1 3/4\\" ', 'P81J.1507 Tub Liso.jpg', '', '', '', '', '', '', '', ''),
(163, 2, 47, 'Canal con aleta 3" x 1"', '', '2899', '', 0, '', 'G54Q.2899 Canal con aleta desc.jpg', '', '', '', '', '', '', '', ''),
(164, 2, 47, 'Canal liso', '', '1773', '', 0, '', 'ZJJ2.1773 Canal Lisa.jpg', '', '', '', '', '', '', '', ''),
(165, 2, 47, 'Tubular con aleta', '', '2900', '', 0, '', 'EDZQ.2900 Tub con aleta desc.jpg', '', '', '', '', '', '', '', ''),
(166, 2, 47, 'Tubular doble aleta', '', '2898', '', 0, '', 'R8FA.2898 Tub con doble aleta desc.jpg', '', '', '', '', '', '', '', ''),
(167, 2, 47, 'Tubular liso', '', '2901', '', 0, '', 'XYBM.2901 Tub liso.jpg', '', '', '', '', '', '', '', ''),
(168, 2, 46, 'Canal ducto electrico', '', '1769', '', 0, '', 'ASKA.1769 Canal Lisa.jpg', '', '', '', '', '', '', '', ''),
(169, 2, 46, 'Canal con aleta', '', '2084', '', 0, '', '228Y.2084 Canal con aleta desc.jpg', '', '', '', '', '', '', '', ''),
(170, 2, 46, 'Tubular doble aleta', '', '2086', '', 0, '', 'DWI3.2086 tub doble aleta desc.jpg', '', '', '', '', '', '', '', ''),
(171, 2, 46, 'Tubular con aleta', '', '2085', '', 0, '', 'HWLC.2085 Tub con aleta desc.jpg', '', '', '', '', '', '', '', ''),
(172, 2, 46, 'Tubular liso', '', '2837', '', 0, '', 'RZVD.2837 tub 3 x 1 1 2 liso.jpg', '', '', '', '', '', '', '', ''),
(173, 2, 45, 'Canal lisa', '', '2079', '', 0, '', 'KSKI.2079 Canal Lisa.jpg', '', '', '', '', '', '', '', ''),
(174, 2, 45, 'Tubular con aleta', '', '2903', '', 0, '', '4SZU.2903 Tubular con aleta desc.jpg', '', '', '', '', '', '', '', ''),
(175, 2, 45, 'Tubular liso', '', '2857', '', 0, '', 'FM1U.2857 Tubular Liso.jpg', '', '', '', '', '', '', '', ''),
(176, 2, 45, 'Canal con aleta', '', '2666', '', 0, '', 'K7M.2666 canal con aleta.jpg', '', '', '', '', '', '', '', ''),
(177, 2, 45, 'Tubular con aleta', '', '2667', '', 0, '', '4J32.2667 tubular con aleta.jpg', '', '', '', '', '', '', '', ''),
(178, 2, 45, 'Tubular con doble aleta', '', '2668', '', 0, '', 'H9E2.2668 Tubular con doble aleta.jpg', '', '', '', '', '', '', '', ''),
(179, 2, 44, 'Base de pasamanos', '', '2320', '', 0, '', 'JSC8.2320 Base Pasamanos.jpg', '', '', '', '', '', '', '', ''),
(180, 2, 44, 'Elipse vertical', '', '2292', '', 0, '', '4DIY.2292 Elipse Vertical.jpg', '', '', '', '', '', '', '', ''),
(181, 2, 44, 'Persiana fija', '', '3027', '', 0, '', 'YSIB.3027 Persiana Fija.jpg', '', '', '', '', '', '', '', ''),
(182, 2, 44, 'Tapa de pasamanos', '', '2321', '', 0, '', 'YWHS.2321 Tapa para base recta pasamanos.jpg', '', '', '', '', '', '', '', ''),
(183, 2, 44, 'Pasamanos base recta', '', '2294', '', 0, '', 'F7DQ.2294 Pasamanos de base recta.jpg', '', '', '', '', '', '', '', ''),
(184, 2, 44, 'Tapa inferior pasamanos', '', '2293', '', 0, '', '1J8T.2293 Tapa Inferior pas.jpg', '', '', '', '', '', '', '', ''),
(185, 2, 43, 'Marco puerta entamborada', '', '2101', '', 0, '', 'ZU5X.2101 marco puerta entamborada.jpg', '', '', '', '', '', '', '', ''),
(186, 2, 43, 'Puerta entamborada tubular', '', '2536', '', 0, '', 'B4N7.2536 puerta entamborada.jpg', '', '', '', '', '', '', '', ''),
(187, 2, 42, 'Adaptador', '', '2003', '', 0, 'Puerta corrediza 2044', 'FRQA.2003 Adaptador.jpg', '', '', '', '', '', '', '', ''),
(188, 2, 42, 'Cabezal', '', '2891', '', 0, '', 'DG6X.2891 Cabezal.jpg', '', '', '', '', '', '', '', ''),
(189, 2, 42, 'Enganche', '', '2890', '', 0, '', '3WRR.2890 Enganche.jpg', '', '', '', '', '', '', '', ''),
(190, 2, 42, 'Horizontal inferior', '', '2894', '', 0, '', 'P7X7.2894 Horizontal inferior.jpg', '', '', '', '', '', '', '', ''),
(191, 2, 42, 'Horizontal superior', '', '2889', '', 0, '', '431X.2889 Horizontal sup.jpg', '', '', '', '', '', '', '', ''),
(192, 2, 42, 'Jamba', '', '2892', '', 0, '', 'WVDF.2892 Jamba.jpg', '', '', '', '', '', '', '', ''),
(193, 2, 42, 'Sillar', '', '1996', '', 0, '', 'CIFD.1996 Sillar.jpg', '', '', '', '', '', '', '', ''),
(194, 2, 42, 'Traslape', '', '2893', '', 0, '', 'NH2A.2893 Traslape.jpg', '', '', '', '', '', '', '', ''),
(195, 2, 41, 'Enganche', '', '2995', '', 0, '', 'RZ6I.2995 Enganche.jpg', '', '', '', '', '', '', '', ''),
(196, 2, 41, 'Adaptador', '', '2996', '', 0, 'Puerta corrediza 2028 Plus ', 'X2G4.2996 Adaptador.jpg', '', '', '', '', '', '', '', ''),
(197, 2, 41, 'Horizontal inferior', '', '2993', '', 0, '', 'V2SP.2993 horizontal inferior.jpg', '', '', '', '', '', '', '', ''),
(198, 2, 41, 'Horizontal superior', '', '2992', '', 0, '', 'LRWY.2992 horizontal superior.jpg', '', '', '', '', '', '', '', ''),
(199, 2, 41, 'Jamba', '', '2991', '', 0, '', 'VHBJ.2991 Jamba.jpg', '', '', '', '', '', '', '', ''),
(200, 2, 41, 'Sillar', '', '2990', '', 0, '', '9BBF.2990 Sillar.jpg', '', '', '', '', '', '', '', ''),
(201, 2, 41, 'Cabezal', '', '2989', '', 0, '', 'TCU.2989 Cabezal.jpg', '', '', '', '', '', '', '', ''),
(202, 2, 41, 'Traslape', '', '2994', '', 0, '', 'C24P.2994 Traslape.jpg', '', '', '', '', '', '', '', ''),
(203, 2, 40, 'Riel superior', '', '1070', '', 0, '', 'TE3F.1070 Riel superior.jpg', '', '', '', '', '', '', '', ''),
(204, 2, 40, 'Entrecierre', '', '1700', '', 0, '', 'Q3F6.1700 Entrecierre.jpg', '', '', '', '', '', '', '', ''),
(205, 2, 40, 'Horizontal superior', '', '1697', '', 0, '', '2SS.1697 horizontal superior.jpg', '', '', '', '', '', '', '', ''),
(206, 2, 40, 'Jamba', '', '1073', '', 0, '', '7F39.1073 Jamba marco.jpg', '', '', '', '', '', '', '', ''),
(207, 2, 40, 'Sillar', '', '1069', '', 0, '', 'EN63.1069 Riel inferior.jpg', '', '', '', '', '', '', '', ''),
(208, 2, 40, 'Horizontal superior', '', '1698', '', 0, '', 'FZN6.1698 horizontal inferior.jpg', '', '', '', '', '', '', '', ''),
(209, 2, 40, 'Traslape', '', '2750', '', 0, '', '3BC8.2750 Vertical de hoja.jpg', '', '', '', '', '', '', '', ''),
(210, 2, 39, 'Adaptador', '', '1876', '', 0, 'Puerta corrediza 2025', 'BRUQ.1876 Adaptador.jpg', '', '', '', '', '', '', '', ''),
(211, 2, 39, 'Horizontal superior', '', '1874', '', 0, '', 'KTTC.1874 Horizontal superior.jpg', '', '', '', '', '', '', '', ''),
(212, 2, 39, 'Jamba', '', '1871', '', 0, '', 'HZIZ.1871 Jamba.jpg', '', '', '', '', '', '', '', ''),
(213, 2, 39, 'Pisador', '', '1877', '', 0, '', '4A1C.1877 Pisador.jpg', '', '', '', '', '', '', '', ''),
(214, 2, 39, 'Enganche', '', '1873', '', 0, '', 'JPLI.1873 Enganche.jpg', '', '', '', '', '', '', '', ''),
(215, 2, 39, 'Horizontal inferior', '', '1875', '', 0, '', '3DV1.1875 Horizontal inferior.jpg', '', '', '', '', '', '', '', ''),
(216, 2, 39, 'Traslape', '', '1872', '', 0, '', 'UVYK.1872 Traslape.jpg', '', '', '', '', '', '', '', ''),
(217, 2, 38, 'Horizontal inferior', '', '2333', '', 0, '', 'EPES.2333 htal inf de hoja.jpg', '', '', '', '', '', '', '', ''),
(218, 2, 38, 'Horizontal superior', '', '2336', '', 0, '', 'CP1U.2336 htal sup de hoja.jpg', '', '', '', '', '', '', '', ''),
(219, 2, 38, 'Pisavidrio', '', '1104', '', 0, '', 'IT1H.1104 Pisavidrio.jpg', '', '', '', '', '', '', '', ''),
(220, 2, 38, 'Vertical felpa', '', '2334', '', 0, '', 'FDSM.2334 Vertical felpa.jpg', '', '', '', '', '', '', '', ''),
(221, 2, 38, 'Vertical liso', '', '2335', '', 0, '', 'VP4.2335 Vertical de hoja.jpg', '', '', '', '', '', '', '', ''),
(222, 2, 36, 'Peinazo mediano', '', '2927', '', 0, '', 'SCV.2927 peinazo Mediano.jpg', '', '', '', '', '', '', '', ''),
(223, 2, 36, 'Pirlan dos rayas', '', '2735', '', 0, '3/4\\"', 'J865.2735 pirlan 3 4.jpg', '', '', '', '', '', '', '', ''),
(224, 2, 36, 'Pirlan dos rayas', '', '2734', '', 0, '1\\"', 'BVM5.2734 Pirlan de 1.jpg', '', '', '', '', '', '', '', ''),
(225, 2, 36, 'Pirlan', '', '1232', '', 0, '1 1/4\\"', 'N8DC.1232 Pirlan de 1 1 4.jpg', '', '', '', '', '', '', '', ''),
(226, 2, 36, 'Pirlan', '', '1233', '', 0, '1\\"', 'CN3U.1233 pirlan de 1.jpg', '', '', '', '', '', '', '', ''),
(227, 2, 35, 'Perfil persiana vertical', '', '2359', '', 0, '', 'JIQF.2359 Perfil persiana vertical.jpg', '', '', '', '', '', '', '', ''),
(228, 2, 36, 'Piñon', '', '1821', '', 0, '', 'QTEG.1821 Piñon.jpg', '', '', '', '', '', '', '', ''),
(229, 2, 34, 'Anjeo puerta corrediza', '', '2332', '', 0, '', 'TRCF.2332 mosq con autoroscante.jpg', '', '', '', '', '', '', '', ''),
(230, 2, 34, 'Angulo z', '', '2103', '', 0, '', 'IVPH.2103 angulo z.jpg', '', '', '', '', '', '', '', ''),
(231, 2, 34, 'Botagua o marco fijo', '', '1392', '', 0, '', 'VLNE.1392 botagua.jpg', '', '', '', '', '', '', '', ''),
(232, 2, 34, 'Botagua', '', '2474', '', 0, 'Marco fijo de 37 x 27', 'QXLA.2474 botagua.jpg', '', '', '', '', '', '', '', ''),
(233, 2, 34, 'Canal de 25 x 15 mm', '', '2098', '', 0, '', '9PZY.2098 canal de 25 x 15.jpg', '', '', '', '', '', '', '', ''),
(234, 2, 34, 'Dilatación', '', '2030', '', 0, '', 'SCSY.2030 dilatacion.jpg', '', '', '', '', '', '', '', ''),
(235, 2, 34, 'Perfil en f', '', '1984', '', 0, 'Para vidrio de 7 mm', 'DPC.1984 f 7 mm.jpg', '', '', '', '', '', '', '', ''),
(236, 2, 34, 'Anjeo', '', '2022', '', 0, '', '9SJ.2022 mosquitero.jpg', '', '', '', '', '', '', '', ''),
(237, 2, 34, 'Perfil persiana', '', '2020', '', 0, '', 'RFYS.2020 persiana.jpg', '', '', '', '', '', '', '', ''),
(238, 2, 34, 'Pisavidrio alamo fijo', '', '2004', '', 0, '', 'FFZU.2004 pisavidrio alamo.jpg', '', '', '', '', '', '', '', ''),
(239, 2, 34, 'Pisavidrio alamo movil', '', '2005', '', 0, '', 'F3B3.2005 pisavidrio alamo movil.jpg', '', '', '', '', '', '', '', ''),
(240, 2, 34, 'Reticula', '', '2490', '', 0, '', '2PH5.2490 reticula.jpg', '', '', '', '', '', '', '', ''),
(241, 2, 34, 'Wing', '', '2819', '', 0, '', 'RA28.2819 wing.jpg', '', '', '', '', '', '', '', ''),
(242, 2, 34, 'Dilatación', '', '2032', '', 0, '', 'HJXA.2032 dilatacion.jpg', '', '', '', '', '', '', '', ''),
(243, 2, 34, 'Rejilla difusora electrica', '', '2475', '', 0, '', 'PPUC.2475 rejilla termodifusora.jpg', '', '', '', '', '', '', '', ''),
(244, 2, 34, 'Tee con dilatación', '', '2102', '', 0, '28 mm x 19 mm', '9U9N.2102 t con dilatacion.jpg', '', '', '', '', '', '', '', ''),
(245, 2, 34, 'Cabezal riel de vitrina', '', '2487', '', 0, '', 'YJDQ.2487 cabezal riel de vitrina.jpg', '', '', '', '', '', '', '', ''),
(246, 2, 34, 'Sillar riel vitrina', '', '2488', '', 0, '', '1XR3.2488 sillar riel de vitrina.jpg', '', '', '', '', '', '', '', ''),
(247, 2, 34, 'Tapa ducto', '', '2485', '', 0, '', 'H5Z6.2485 tapa ducto 1 1 2.jpg', '', '', '', '', '', '', '', ''),
(248, 2, 33, 'Soporte post escalera', '', '2162', '', 0, '', 'EFNS.2162 soporte post.jpg', '', '', '', '', '', '', '', ''),
(249, 2, 33, 'Travezaño escalera', '', '2309', '', 0, '', '3L4I.2309 soporte post.jpg', '', '', '', '', '', '', '', ''),
(250, 2, 33, 'Canal divisor de oficina', '', '2524', '', 0, '', 'ECF8.2524 canal de 1  1 2.jpg', '', '', '', '', '', '', '', ''),
(251, 2, 33, 'Divisor de oficna', '', '1591', '', 0, '34 x 20\\"', 'ZM75.1591 U 34 x 20.jpg', '', '', '', '', '', '', '', ''),
(252, 2, 33, 'Divisor de oficina', '', '2896', '', 0, '38\\" x 30\\"', '463K.2896 U 38 x 30.jpg', '', '', '', '', '', '', '', ''),
(253, 2, 32, 'Tee con dilatación', '', '2102', '', 0, '28 mm x 19 mm', 'UH11.2102 t con dilatacion.jpg', '', '', '', '', '', '', '', ''),
(254, 2, 31, 'División de oficina', '', '2897', '', 0, '', 'LBN1.2897 H 34 x 30.jpg', '', '', '', '', '', '', '', ''),
(255, 2, 30, 'Camisa vertical', '', '1197', '', 0, '', '8BV.1197 Camisa vertical.jpg', '', '', '', '', '', '', '', ''),
(256, 2, 29, 'Doble peinazo', '', '2062', '', 0, '', 'XPXR.2062 Doble Peinazo.jpg', '', '', '', '', '', '', '', ''),
(257, 2, 29, 'Peinazo grande', '', '1404', '', 0, '', 'L6KF.1404 Peinazo Grande.jpg', '', '', '', '', '', '', '', ''),
(258, 2, 29, 'Peinazo pequeño', '', '2928', '', 0, '', '44SW.2928 Peinazo Pequeño.jpg', '', '', '', '', '', '', '', ''),
(259, 1, 9, 'Adaptador de nave diseño OXXO', '', '3426', '', 0, 'Puerta corrediza Colosal 345', '1ZGU.3426 adaptador de nave OXXO.jpg', '', '', '', '', '', '', '', ''),
(260, 1, 9, 'Adaptador de riel', '', '3419', '', 0, 'Puerta corrediza Colosal 345', 'LMFL.3419 Adaptador de riel.jpg', '', '', '', '', '', '', '', ''),
(261, 1, 9, 'Doble entrecierre', '', '3427', '', 0, '', '5Q4W.3427 Doble entrecierre.jpg', '', '', '', '', '', '', '', ''),
(262, 1, 9, 'Entrecierre', '', '3421', '', 0, '', '378.3421 Entrecierre.jpg', '', '', '', '', '', '', '', ''),
(263, 1, 9, 'Marco perimetral de nave', '', '3420', '', 0, '', 'NBXV.3420 Marco perimetral de nave.jpg', '', '', '', '', '', '', '', ''),
(264, 1, 9, 'Marco perimetral', '', '3418', '', 0, '', 'Z8.3418 Marco perimetral.jpg', '', '', '', '', '', '', '', ''),
(265, 1, 9, 'Marco perimetral de anjeo', '', '2331', '', 0, '', 'WM5R.2331 Marco perimetral de anjeo.jpg', '', '', '', '', '', '', '', ''),
(266, 1, 7, 'Pisador', '', '3206', '', 0, '', '141U.3206 Pisador.jpg', '', '', '', '', '', '', '', ''),
(267, 1, 7, 'Anjeo perimetral', '', '3196', '', 0, '', '65P8.3196 Anjeo perimetral.jpg', '', '', '', '', '', '', '', ''),
(268, 1, 7, 'Complemento jamba', '', '3387', '', 0, '', 'J957.3387 Complemento Jamba.jpg', '', '', '', '', '', '', '', ''),
(269, 1, 7, 'Complemento riel superior', '', '3386', '', 0, '', '6ISS.3386 Complemento riel sup.jpg', '', '', '', '', '', '', '', ''),
(270, 1, 7, 'Divisor anjeo', '', '3218', '', 0, '', 'BS24.3218 Divisor anjeo.jpg', '', '', '', '', '', '', '', ''),
(271, 1, 7, 'Enganche', '', '3216', '', 0, '', 'NETQ.3216 Enganche.jpg', '', '', '', '', '', '', '', ''),
(272, 1, 7, 'Enganche reforzado', '', '3385', '', 0, '', '5RIC.3385 Entrecierre reforzado.jpg', '', '', '', '', '', '', '', ''),
(273, 1, 7, 'Guia doble', '', '3210', '', 0, '', 'TNID.3210 Guía doble.jpg', '', '', '', '', '', '', '', ''),
(274, 1, 7, 'Guia simple', '', '3209', '', 0, '', 'JIXQ.3209 Guia simple.jpg', '', '', '', '', '', '', '', ''),
(275, 1, 7, 'Horizontal inferior cuerpo fijo', '', '3213', '', 0, '', '3J41.3213 htal inf fijo.jpg', '', '', '', '', '', '', '', ''),
(276, 1, 7, 'Horizontal inferior nave', '', '3412', '', 0, '', 'L4AX.3214 Htal inf nave.jpg', '', '', '', '', '', '', '', ''),
(277, 1, 7, 'Jamba', '', '3211', '', 0, '', 'GDCT.3211 Jamba Marco.jpg', '', '', '', '', '', '', '', ''),
(278, 1, 8, 'Jamba Puerta Satelital', '', '3384', '', 0, '', '47AY.3384 Jamba.jpg', '', '', '', '', '', '', '', ''),
(279, 1, 7, 'Riel inferior anjeo', '', '3205', '', 0, '', 'U5I.3205 Riel inferior de anjeo.jpg', '', '', '', '', '', '', '', ''),
(280, 1, 7, 'Riel inferior', '', '3208', '', 0, '', '56GF.3208 Riel inferior.jpg', '', '', '', '', '', '', '', ''),
(281, 1, 7, 'Riel inferior Puerta Satelital', '', '3383', '', 0, '', 'VJW.3383 Riel inferior.jpg', '', '', '', '', '', '', '', ''),
(282, 1, 7, 'Riel superior de anjeo', '', '3204', '', 0, '', 'H7MH.3204 Riel superior de anjeo.jpg', '', '', '', '', '', '', '', ''),
(283, 1, 7, 'Riel superior Puerta Satelital', '', '3382', '', 0, '', 'DR1U.3382 Riel sup.jpg', '', '', '', '', '', '', '', ''),
(284, 1, 7, 'Cabezal', '', '3207', '', 0, '', '8ZXA.3207 Cabezal.jpg', '', '', '', '', '', '', '', ''),
(285, 1, 7, 'Horizontal superior', '', '3212', '', 0, '', 'PV4L.3212 Horizontal sup.jpg', '', '', '', '', '', '', '', ''),
(286, 1, 7, 'Vertical de cerradura', '', '3215', '', 0, '', 'XRCM.3215 Vertical de cerradura.jpg', '', '', '', '', '', '', '', ''),
(287, 1, 6, 'Anjeo perimetral', '', '3196', '', 0, '', 'CNBF.3196 Anjeo perimetral.jpg', '', '', '', '', '', '', '', ''),
(288, 1, 6, 'Divisor anjeo', '', '3218', '', 0, '', 'IS8.3218 Divisor anjeo.jpg', '', '', '', '', '', '', '', ''),
(289, 1, 6, 'Entrecierre reforzado', '', '3203', '', 0, '', 'NZ19.3203 Entrecierre reforzado.jpg', '', '', '', '', '', '', '', ''),
(290, 1, 6, 'Enganche', '', '3195', '', 0, '', '8B28.3195 Enganche.jpg', '', '', '', '', '', '', '', ''),
(291, 1, 6, 'Jamba', '', '3201', '', 0, '', 'Z2AM.3201 jamba marco tres pistas.jpg', '', '', '', '', '', '', '', ''),
(292, 1, 6, 'Jamba marco', '', '3191', '', 0, '', '46PU.3191 Jamba marco.jpg', '', '', '', '', '', '', '', ''),
(293, 1, 6, 'Jamba nave', '', '3191', '', 0, '', 'J3UF.3191 Jamba marco.jpg', '', '', '', '', '', '', '', ''),
(294, 1, 6, 'Regilla anticondensación', '', '3189', '', 0, '', '869W.3189 Rejilla anticondensación.jpg', '', '', '', '', '', '', '', ''),
(295, 1, 6, 'Riel inferior de anjeo', '', '3205', '', 0, '', '43Y4.3205 Riel inferior de anjeo.jpg', '', '', '', '', '', '', '', ''),
(296, 1, 6, 'Riel inferior de marco', '', '3200', '', 0, '', 'MHCU.3200 Riel inferior de marco.jpg', '', '', '', '', '', '', '', ''),
(297, 1, 6, 'Riel superior de anjeo', '', '3204', '', 0, '', 'U142.3204 Riel superior de anjeo.jpg', '', '', '', '', '', '', '', ''),
(298, 1, 6, 'Riel superior', '', '3199', '', 0, '', 'I8JE.3199 Riel superior.jpg', '', '', '', '', '', '', '', ''),
(299, 1, 6, 'Horizontal superior nave', '', '3202', '', 0, '', '88EY.3202 Horizontal superior.jpg', '', '', '', '', '', '', '', ''),
(300, 1, 6, 'Vertical de cerradura', '', '3194', '', 0, '', '2NE.3194 Vertical de cerradura.jpg', '', '', '', '', '', '', '', ''),
(301, 1, 8, 'Adaptador felpa', '', '3223', '', 0, 'Puerta batiente Sideral 2.4', '7UI6.3223 Adapta felpa.jpg', '', '', '', '', '', '', '', ''),
(302, 1, 8, 'Adaptador', '', '3224', '', 0, 'Puerta batiente Sideral 2.4', '1JN4.3224 Adaptador.jpg', '', '', '', '', '', '', '', ''),
(303, 1, 8, 'Divisor', '', '3261', '', 0, '', '3EA.3261 Divisor.jpg', '', '', '', '', '', '', '', ''),
(304, 1, 8, 'Marco perimetral', '', '3219', '', 0, '', 'M5E5.3219 Marco perimetral.jpg', '', '', '', '', '', '', '', ''),
(305, 1, 8, 'Horizontal superior nave', '', '3221', '', 0, '', 'HKND.3221 Superior nave.jpg', '', '', '', '', '', '', '', ''),
(306, 1, 8, 'PIsavidrio curvo', '', '3295', '', 0, '', 'L5SM.3295 Pisavidrio curvo.jpg', '', '', '', '', '', '', '', ''),
(307, 1, 8, 'Pisavidrio', '', '3262', '', 0, '', 'GNK3.3262 Pisavidrio.jpg', '', '', '', '', '', '', '', ''),
(308, 1, 8, 'Vertical bisagra', '', '3220', '', 0, '', '9ZC5.3220 vertical bisagra.jpg', '', '', '', '', '', '', '', ''),
(309, 1, 8, 'Vertical de cerradura', '', '3388', '', 0, '', '1V6A.3388 Vertical de cerradura.jpg', '', '', '', '', '', '', '', ''),
(310, 2, 28, 'Pasamanos', '', '2823', '', 0, '2\\" x 1/2\\"', 'UKGQ.2823 pasamanos de 2 x 1 12.jpg', '', '', '', '', '', '', '', ''),
(311, 2, 28, 'Pasamanos', '', '2572', '', 0, '2\\" 3/4 x 1\\" ', '8CG5.2572 Pasamanos.jpg', '', '', '', '', '', '', '', ''),
(312, 2, 28, 'Pasamanos', '', '2393', '', 0, '50.8 mm x 38.1 mm', 'KDJN.2393 pasamnos de 2 x 1 1 2.jpg', '', '', '', '', '', '', '', ''),
(313, 2, 28, 'Pasamanos', '', '2068', '', 0, '', '1USG.2068 Pasamanos.jpg', '', '', '', '', '', '', '', ''),
(314, 2, 28, 'Pasamanos', '', '2602', '', 0, '', 'GPS8.2602 Pasamanos vidrio templado.jpg', '', '', '', '', '', '', '', ''),
(315, 2, 28, 'Mangon ovalado', '', '1415', '', 0, '', 'WAW3.1415 Mangon Ovalado.jpg', '', '', '', '', '', '', '', ''),
(316, 2, 28, 'Paral de pasamanos', '', '1497', '', 0, '', '4TAG.1497 Paral de Pasamanos.jpg', '', '', '', '', '', '', '', ''),
(317, 2, 28, 'Tubo ovalado', '', '1601', '', 0, '', 'TXR7.1601 Tubo Ovalado.jpg', '', '', '', '', '', '', '', ''),
(318, 2, 28, 'Mangon pasamanos', '', '1856', '', 0, '', '62J1.1856 Mangon Pasamanos.jpg', '', '', '', '', '', '', '', ''),
(320, 2, 26, 'Malayo mediano', '', '2457', '', 0, '', 'PWT.2457 malayo mediano.jpg', '', '', '', '', '', '', '', ''),
(321, 2, 26, 'Malayo pequeño', '', '2458', '', 0, '', '583T.2458 malayo pño.jpg', '', '', '', '', '', '', '', ''),
(322, 2, 26, 'Malyo Vitral', '', '2905', '', 0, '', 'AFHN.2905 malayo vitral.jpg', '', '', '', '', '', '', '', ''),
(323, 2, 26, 'Manija', '', '2998', '', 0, '', 'YKX.2998 manija.jpg', '', '', '', '', '', '', '', ''),
(324, 2, 26, 'Manija', '', '2534', '', 0, '55 mm', 'T64T.2534 manija.jpg', '', '', '', '', '', '', '', ''),
(325, 2, 26, 'Soporte manija', '', '2997', '', 0, '', '2M1E.2997 soporte manija.jpg', '', '', '', '', '', '', '', ''),
(326, 2, 26, 'Tiradera', '', '1140', '', 0, '', '3LLX.1140 tiradera.jpg', '', '', '', '', '', '', '', ''),
(327, 2, 26, 'Tiradera', '', '1363', '', 0, '', 'QS7.1363 tiradera.jpg', '', '', '', '', '', '', '', ''),
(328, 2, 26, 'Tiradera puerta americana', '', '1324', '', 0, '', 'T2QW.1324 tiradera.jpg', '', '', '', '', '', '', '', ''),
(329, 2, 26, 'Tiradera', '', '2176', '', 0, '', '9D8F.2176 tiradera.jpg', '', '', '', '', '', '', '', ''),
(330, 2, 25, 'Adaptador marco de vitrina 3 x 1 s 201 (3" x 1 1/2")', '', '2427', '', 0, 'Mamparas', 'FSM.2427 tapa con bolsillo.jpg', '', '', '', '', '', '', '', ''),
(331, 2, 25, 'Guia autoroscante de vitrina', '', '2441', '', 0, 's 201 (3\\" x 1 1/2\\")', 'ZS7C.2441 Ancla.jpg', '', '', '', '', '', '', '', ''),
(332, 2, 25, 'Horizontal de vitrina', '', '2428', '', 0, 's 201 (3\\" x 1 1/2\\")', 'BG73.2428 Horizontal.jpg', '', '', '', '', '', '', '', ''),
(333, 2, 25, 'Pisavidrio', '', '2429', '', 0, '7 s 201 (3\\" x 1 1/2\\")', 'HE7R.2429 Pisavidrio.jpg', '', '', '', '', '', '', '', ''),
(334, 2, 25, 'Tapa vertical fachada', '', '1645', '', 0, ' s 201 (3\\" x 1 1/2\\")', '7GND.1645 Tapa para marco.jpg', '', '', '', '', '', '', '', ''),
(335, 2, 25, 'Vertical de vitrina', '', '2426', '', 0, '7 s 201 (3\\" x 1 1/2\\")', '44ME.2426 Vertical.jpg', '', '', '', '', '', '', '', ''),
(336, 2, 25, 'Tapa con aleta para marco de puerta', '', '1688', '', 0, 's 201 (3\\" x 1 1/2\\")', '8REJ.1688 tapa con aleta.jpg', '', '', '', '', '', '', '', ''),
(337, 2, 25, 'Horizontal para vitrina', '', '2432', '', 0, 's 203 (3\\" x 1 1/2\\")', 'W2G1.2432 Horizontal.jpg', '', '', '', '', '', '', '', ''),
(338, 2, 25, 'Pisavidrio para vitrina', '', '2433', '', 0, ' s 203 (3\\" x 1 1/2\\")', 'Y346.2433 Pisavidrio.jpg', '', '', '', '', '', '', '', ''),
(339, 2, 25, 'Tapa vitrina', '', '2431', '', 0, 's 203 (3\\" x 1 1/2\\")', '9VP.2431 tapa con bolsillo.jpg', '', '', '', '', '', '', '', ''),
(340, 2, 25, 'Vertical vitrina', '', '2430', '', 0, 's 203 (3\\" x 1 1/2\\")', 'AG7.2430 Vertical.jpg', '', '', '', '', '', '', '', ''),
(341, 2, 24, 'Canal fachada flotante', '', '2076', '', 0, '5050', 'FR8.2076 Contratapa.jpg', '', '', '', '', '', '', '', ''),
(342, 2, 24, 'Clip horizontal', '', '1198', '', 0, '', 'BSE4.1198 Clip horizontal.jpg', '', '', '', '', '', '', '', ''),
(343, 2, 24, 'Espalda', '', '1196', '', 0, '', '5NCI.1196 Espalda.jpg', '', '', '', '', '', '', '', ''),
(344, 2, 24, 'Tapa de vidrio', '', '2075', '', 0, '', 'D75T.2075 Tapa de vidrio.jpg', '', '', '', '', '', '', '', ''),
(345, 2, 24, 'Camisa vertical', '', '1197', '', 0, '', 'WA2F.1197 Camisa vertical.jpg', '', '', '', '', '', '', '', ''),
(346, 2, 24, 'Contratapa plana', '', '1199', '', 0, '', 'TH9R.1199 Contratapa.jpg', '', '', '', '', '', '', '', ''),
(347, 2, 25, 'Tapa de vidrio', '', '1200', '', 0, '', '9QZL.1200 Tapa de vidrio.jpg', '', '', '', '', '', '', '', ''),
(348, 2, 24, 'Remate tapajunta', '', '2214', '', 0, '', 'XIPB.2214 Remate tapajunta.jpg', '', '', '', '', '', '', '', ''),
(349, 2, 24, 'Tapajunta', '', '2215', '', 0, '', 'TQUH.2215 tapajunta.jpg', '', '', '', '', '', '', '', ''),
(350, 2, 24, 'Contratapa', '', '2963', '', 0, '', 'YCP3.2936 Contratapa.jpg', '', '', '', '', '', '', '', ''),
(351, 2, 24, 'Basculante', '', '1560', '', 0, '', 'JZDA.1560 Basculante.jpg', '', '', '', '', '', '', '', ''),
(352, 2, 24, 'Pisavidrio', '', '1558', '', 0, '', '7PM.1558 Pisavidrio.jpg', '', '', '', '', '', '', '', ''),
(353, 2, 24, 'Extensión vertical', '', '1562', '', 0, '', '948P.1562 Extensión Vertical.jpg', '', '', '', '', '', '', '', ''),
(354, 2, 24, 'Telescopico', '', '1559', '', 0, '', '81RR.1559 Telescopico.jpg', '', '', '', '', '', '', '', ''),
(355, 2, 24, 'Vertical', '', '1557', '', 0, '', 'HXU9.1557 Vertical.jpg', '', '', '', '', '', '', '', ''),
(356, 2, 24, 'Horizontal', '', '1556', '', 0, '', 'C7TZ.1556 Horizontal.jpg', '', '', '', '', '', '', '', ''),
(357, 2, 24, 'Contrafuego horizontal', '', '1561', '', 0, '', '935G.1561 Cortafuego Horizontal.jpg', '', '', '', '', '', '', '', ''),
(358, 2, 23, 'Paral escalera extension grande', '', '2124', '', 0, '', '28A.2124 paral escalera ext grande.jpg', '', '', '', '', '', '', '', ''),
(359, 2, 23, 'Paral escalera extensíon pequeña', '', '2116', '', 0, '', 'TZES.2116 para extension escalera pña.jpg', '', '', '', '', '', '', '', ''),
(360, 2, 23, 'Peldaño circular', '', '2118', '', 0, '', '474Z.2118 peldaño circular esc extension.jpg', '', '', '', '', '', '', '', ''),
(361, 2, 23, 'Peldaño escalera en D', '', '2948', '', 0, '', '5L8.2948 peldaño esc en D.jpg', '', '', '', '', '', '', '', ''),
(362, 2, 23, 'Angulo de fijación universal', '', '2120', '', 0, '', 'UDQY.2120 angulo de fijacion universal.jpg', '', '', '', '', '', '', '', ''),
(363, 2, 23, 'Paral de escalera liviana', '', '2123', '', 0, '', 'MHWF.2123 paral tras.jpg', '', '', '', '', '', '', '', ''),
(364, 2, 23, 'Paral delantero escalera grande', '', '2163', '', 0, '', 'QY1W.2163 paral delantero.jpg', '', '', '', '', '', '', '', ''),
(365, 2, 23, 'Paral delantero escalera mediana', '', '2121', '', 0, '', 'CH1K.2121 paral delantero esc pña.jpg', '', '', '', '', '', '', '', ''),
(366, 2, 23, 'Paral trasero escalera grande', '', '2164', '', 0, '', 'VEIV.2164 paral trasero.jpg', '', '', '', '', '', '', '', ''),
(367, 2, 23, 'Peldaño escalera grande', '', '2122', '', 0, '', 'MMRZ.2122 peldaño esc grande.jpg', '', '', '', '', '', '', '', ''),
(368, 2, 23, 'Peldaño escalera mediana', '', '2117', '', 0, '', '7WHD.2117 peldaño esc pña.jpg', '', '', '', '', '', '', '', ''),
(369, 2, 23, 'Piso superior escalera', '', '1659', '', 0, '', '6DGY.1659 piso superior.jpg', '', '', '', '', '', '', '', ''),
(370, 2, 23, 'Soporte posterior escalera grande', '', '2162', '', 0, '', 'F7TD.2162 soporte post.jpg', '', '', '', '', '', '', '', ''),
(371, 2, 23, 'Tijera escalera grande', '', '2165', '', 0, '', 'DI2F.2165 tijera escalera.jpg', '', '', '', '', '', '', '', ''),
(372, 2, 23, 'Tijera escalera mediana', '', '1661', '', 0, '', '3D6B.1661 Tijera escalera.jpg', '', '', '', '', '', '', '', ''),
(373, 2, 23, 'Travezaño escalera liviana', '', '2309', '', 0, '', 'Z5AJ.2309 soporte post.jpg', '', '', '', '', '', '', '', ''),
(374, 2, 22, 'Enchape', '', '3235', '', 0, '', 'S3HV.3235 enchape.jpg', '', '', '', '', '', '', '', ''),
(375, 2, 22, 'Enchape de 13', '', '2291', '', 0, '', 'DI22.2291 enchape 136.1.jpg', '', '', '', '', '', '', '', ''),
(376, 2, 22, 'Enchape de 9', '', '1323', '', 0, '', '6CK2.1323 enchape 96.7.jpg', '', '', '', '', '', '', '', ''),
(377, 2, 22, 'Enchape de 9', '', '1424', '', 0, '', 'WGK.1424 enchape 96.62.jpg', '', '', '', '', '', '', '', ''),
(378, 2, 21, 'Marco de puerta entamborada', '', '2101', '', 0, '', 'B1Q5.2101 marco puerta entamborada.jpg', '', '', '', '', '', '', '', ''),
(379, 2, 21, 'Tapa ducto', '', '2485', '', 0, '', 'LDH7.2485 tapa ducto 1 1 2.jpg', '', '', '', '', '', '', '', ''),
(380, 2, 21, 'Ducto electrico ', '', '2486', '', 0, '3 X 1.1/2', 'XFD.2486 ducto elect sep des 3 x 1 1 2.jpg', '', '', '', '', '', '', '', ''),
(381, 2, 21, 'Ducto autorroscante de 7', '', '2329', '', 0, '', 'JDE2.2329 ducto autoroscante 3 x 1 1 2.jpg', '', '', '', '', '', '', '', ''),
(382, 2, 21, 'tapa ducto electrico de 7', '', '2328', '', 0, '', 'IIW.2328 tapa ducto de 3 x 1 1 2.jpg', '', '', '', '', '', '', '', ''),
(383, 2, 21, 'Tapa perfil electrico', '', '1928', '', 0, '', 'UG.1928 tapa perfil electrico.jpg', '', '', '', '', '', '', '', ''),
(384, 2, 21, 'Ducto electrico', '', '2931', '', 0, '4 x 1 1/2', '1N2Q.2931 ducto 4 x 1 1 2.jpg', '', '', '', '', '', '', '', ''),
(385, 2, 21, 'Tapa ducto', '', '2932', '', 0, '4 x 1 1/2', '57PN.2932 tapa ducto 4 x 1 1 2.jpg', '', '', '', '', '', '', '', ''),
(386, 2, 21, 'Portacables', '', '2019', '', 0, '', 'A854.2019 ducto 4 x 1 3 4.jpg', '', '', '', '', '', '', '', ''),
(387, 2, 21, 'tapa ducto electrico', '', '1369', '', 0, '4\\" x 1 3/4\\"', 'XZY6.1369 tapa ducto 4 x 1 3 4.jpg', '', '', '', '', '', '', '', ''),
(388, 2, 19, 'Estrella de 30 x 30', '', '2545', '', 0, '', 'WNMU.2545 estrella de 30 x 30.jpg', '', '', '', '', '', '', '', ''),
(389, 2, 19, 'Remate curvo de 30 mm', '', '1919', '', 0, '', 'NNT.1919 remate curvo de 30 mm.jpg', '', '', '', '', '', '', '', ''),
(390, 2, 19, 'Estrella de 32 x 32', '', '2295', '', 0, '', '36U.2295 estrella de 32 x 32.jpg', '', '', '', '', '', '', '', ''),
(391, 2, 19, 'Remate curvo de 32 mm', '', '1975', '', 0, '', 'XSCP.1975 remate curvo de 30 mm.jpg', '', '', '', '', '', '', '', ''),
(392, 2, 19, 'Estrella de 32 x 32', '', '1959', '', 0, '', 'F2XT.1959 estrella 32 x 32.jpg', '', '', '', '', '', '', '', ''),
(394, 2, 19, 'Remate horizontal 32 x 12', '', '1961', '', 0, '', 'BKHX.1961 perfil en C.jpg', '', '', '', '', '', '', '', ''),
(395, 2, 19, 'Tubular esquinero 32 x 32', '', '1979', '', 0, '', 'JVXP.1979 tubular esquinero de 32 x 32.jpg', '', '', '', '', '', '', '', ''),
(397, 2, 19, 'Esquinero 35 x 3', '', '2540', '', 0, '', 'DLD6.2540 esquinero.jpg', '', '', '', '', '', '', '', ''),
(398, 2, 19, 'Estrella tubular 35 x 35', '', '2963', '', 0, '', '28BP.2963 estrella tubular.jpg', '', '', '', '', '', '', '', ''),
(399, 2, 19, 'Estrella solida 35 x 35', '', '2538', '', 0, '', '5HX6.2538 estrella abierta.jpg', '', '', '', '', '', '', '', ''),
(400, 2, 19, 'Paral 35 x 70', '', '1585', '', 0, '', '7YDJ.1585 para de 35 x 70.jpg', '', '', '', '', '', '', '', ''),
(401, 2, 19, 'Division de oficina ', '', '1890', '', 0, '44 mm x 25 mm', 'GMCD.1890 estrella arista cuadrada.jpg', '', '', '', '', '', '', '', ''),
(402, 2, 19, 'Estrella arista redonda', '', '2141', '', 0, '44 x 25', 'K3YG.2141 estrella arista redonda.jpg', '', '', '', '', '', '', '', ''),
(403, 2, 19, 'Media estrella', '', '2589', '', 0, '', 'WNEN.2589 media estrella.jpg', '', '', '', '', '', '', '', ''),
(404, 2, 19, 'Media estrella', '', '1835', '', 0, '', 'J5YB.1835 media estrella.jpg', '', '', '', '', '', '', '', ''),
(405, 2, 19, 'Terminal remate en C', '', '2026', '', 0, '', 'YPIF.2026 C terminal remate.jpg', '', '', '', '', '', '', '', ''),
(406, 2, 19, 'Division en H', '', '2897', '', 0, '', 'N699.2897 H 34 x 30.jpg', '', '', '', '', '', '', '', ''),
(407, 2, 19, 'Marco puerta', '', '2281', '', 0, '', 'AM8U.2281  marco puerta.jpg', '', '', '', '', '', '', '', ''),
(408, 2, 19, 'Division de oficina', '', '2524', '', 0, '', '12G1.2524 canal de 1  1 2.jpg', '', '', '', '', '', '', '', ''),
(409, 2, 19, 'Division de oficina en U', '', '2896', '', 0, '', 'SDK3.2896 U 38 x 30.jpg', '', '', '', '', '', '', '', ''),
(410, 2, 19, 'Division de oficna en U', '', '1591', '', 0, '', '2VG.1591 U 34 x 20.jpg', '', '', '', '', '', '', '', ''),
(411, 2, 20, 'Bisagra hembra', '', '1934', '', 0, '', 'HRLS.1934 bisgra hembra division oficina.jpg', '', '', '', '', '', '', '', ''),
(412, 2, 20, 'Bisagra macho', '', '1935', '', 0, '', 'LK43.1935 bisagra macho division de oficina.jpg', '', '', '', '', '', '', '', ''),
(413, 2, 20, 'Paral', '', '1932', '', 0, '', 'FVZ5.1932 paral.jpg', '', '', '', '', '', '', '', ''),
(414, 2, 20, 'Poste octagonal', '', '1933', '', 0, '', 'JAA2.1933 poste octogonal.jpg', '', '', '', '', '', '', '', ''),
(415, 2, 18, 'Barra toallero', '', '2479', '', 0, '', 'QQ67.2479 barra toallero.jpg', '', '', '', '', '', '', '', '');
INSERT INTO `cms_products` (`products_id`, `products_id_cat`, `products_id_subcat`, `products_title`, `products_price`, `products_ref`, `products_size`, `products_stared`, `products_description`, `products_image1`, `products_image2`, `products_image3`, `products_image4`, `products_color_image1`, `products_color_image2`, `products_color_image3`, `products_color_image4`, `products_color_image5`) VALUES
(416, 2, 18, 'Cabezal de baño Vitral', '', '3151', '', 0, '', 'S6UP.3151 cabezal de baño vitral.jpg', '', '', '', '', '', '', '', ''),
(417, 2, 18, 'Cabezal', '', '2408', '', 0, '', 'EW7M.2408 cabezal.jpg', '', '', '', '', '', '', '', ''),
(418, 2, 18, 'Cabezal', '', '2814', '', 0, '', 'KA4.2814 cabezal.jpg', '', '', '', '', '', '', '', ''),
(419, 2, 18, 'Horizontal inferior', '', '2288', '', 0, '', 'JNGP.2288 horizontal inferior.jpg', '', '', '', '', '', '', '', ''),
(420, 2, 18, 'Horizontal superior', '', '2287', '', 0, '', 'WGL.2287 horizontal superior.jpg', '', '', '', '', '', '', '', ''),
(421, 2, 18, 'Jamba', '', '2414', '', 0, '', '9QWA.2414 jamba.jpg', '', '', '', '', '', '', '', ''),
(422, 2, 18, 'Jamba', '', '2285', '', 0, '', 'UCZI.2285 jamba.jpg', '', '', '', '', '', '', '', ''),
(423, 2, 18, 'Manija de sobreponer', '', '2307', '', 0, '', '4KX.2307 manija de sobreponer.jpg', '', '', '', '', '', '', '', ''),
(424, 2, 18, 'Sillar', '', '2283', '', 0, '', 'FHE6.2283 sillar.jpg', '', '', '', '', '', '', '', ''),
(425, 2, 18, 'Soporte toallero', '', '2478', '', 0, '', 'I5YK.2478 soporte toallero.jpg', '', '', '', '', '', '', '', ''),
(426, 2, 18, 'Vertical de la nave', '', '2528', '', 0, '', 'BGEK.2528 vertical de nave.jpg', '', '', '', '', '', '', '', ''),
(427, 2, 18, 'Barra toallero', '', '1630', '', 0, '', 'SHLX.1630 barra toallero.jpg', '', '', '', '', '', '', '', ''),
(428, 2, 18, 'Cabezal', '', '1623', '', 0, '', 'YAZQ.1623 cabezal.jpg', '', '', '', '', '', '', '', ''),
(429, 2, 18, 'Horizontal superior', '', '1626', '', 0, '', '7ABK.1626 horizontal superior.jpg', '', '', '', '', '', '', '', ''),
(430, 2, 18, 'Sillar', '', '1624', '', 0, '', '1J5.1624 sillar.jpg', '', '', '', '', '', '', '', ''),
(431, 2, 18, 'Jamba', '', '1625', '', 0, '', 'LZZ.1625 jamba.jpg', '', '', '', '', '', '', '', ''),
(432, 2, 18, 'Soporte toallero', '', '1629', '', 0, '', 'M63.1629 soporte toallero.jpg', '', '', '', '', '', '', '', ''),
(433, 2, 18, 'Vertical de nave', '', '1627', '', 0, '', 'XAT.1627 vertical de nave.jpg', '', '', '', '', '', '', '', ''),
(434, 2, 18, 'Horizontal inferior', '', '1628', '', 0, '', 'NNAY.1628 horizontal inferior.jpg', '', '', '', '', '', '', '', ''),
(435, 2, 18, 'U de baño', '', '1831', '', 0, '', 'FD2.1831 U de baño.jpg', '', '', '', '', '', '', '', ''),
(436, 2, 18, 'Carrilera', '', '1830', '', 0, '', 'HLL.1830 carrilera.jpg', '', '', '', '', '', '', '', ''),
(437, 2, 18, 'U de baño', '', '2412', '', 0, '', '9QMG.2412 U de baño.jpg', '', '', '', '', '', '', '', ''),
(438, 2, 18, 'Zocalo de 9 cm', '', '1816', '', 0, 'División de baño', 'X6EF.1816 Zocalo de 9 cm.jpg', '', '', '', '', '', '', '', ''),
(439, 2, 18, 'Carrilera', '', '2815', '', 0, '', 'AKSH.2815 carrilera.jpg', '', '', '', '', '', '', '', ''),
(440, 2, 17, 'Bocel doble vena', '', '1654', '', 0, '', '6AWR.1654 bocel doble vena.jpg', '', '', '', '', '', '', '', ''),
(441, 2, 17, 'Bocel externo techo bus', '', '2194', '', 0, '', 'P8FY.2194 bocel externo techo bus.jpg', '', '', '', '', '', '', '', ''),
(442, 2, 17, 'Bocel interno bus', '', '2373', '', 0, '', 'E8ZY.2373 bocel interno bus.jpg', '', '', '', '', '', '', '', ''),
(443, 2, 17, 'Bocel interno techo', '', '2146', '', 0, '', '5SD1.2146 bocel interno techo.jpg', '', '', '', '', '', '', '', ''),
(444, 2, 17, 'Bocel superior', '', '2193', '', 0, '', '3T5I.2193 bocel superior.jpg', '', '', '', '', '', '', '', ''),
(445, 2, 17, 'Bocel aga', '', '2147', '', 0, '', 'Q2.2147 bocel aga.jpg', '', '', '', '', '', '', '', ''),
(446, 2, 17, 'Enchape externo', '', '2149', '', 0, '', 'AURQ.2149 enchape externo.jpg', '', '', '', '', '', '', '', ''),
(447, 2, 17, 'Enchape externo', '', '2149', '', 0, '', 'HB8I.2149 enchape externo.jpg', '', '', '', '', '', '', '', ''),
(448, 2, 17, 'Enchape externo', '', '2149', '', 0, '', '5DJJ.2149 enchape externo.jpg', '', '', '', '', '', '', '', ''),
(449, 2, 17, 'Enchape externo', '', '2149', '', 0, '', '8M4Q.2149 enchape externo.jpg', '', '', '', '', '', '', '', ''),
(450, 2, 17, 'Marco ventanería', '', '2145', '', 0, '', 'IXKI.2145 marco ventaneria.jpg', '', '', '', '', '', '', '', ''),
(451, 2, 17, 'Platina piso superior', '', '1655', '', 0, '', 'V5JY.1655 platina piso superior.jpg', '', '', '', '', '', '', '', ''),
(452, 2, 17, 'Bocel taparueda', '', '2148', '', 0, '', '59B3.2148 bocel taparueda.jpg', '', '', '', '', '', '', '', ''),
(453, 2, 17, 'Bocel transandino', '', '2195', '', 0, '', 'HKG9.2195 bocel transandino.jpg', '', '', '', '', '', '', '', ''),
(454, 2, 17, 'Pasamanos bus', '', '2192', '', 0, '', 'PV23.2192 pasamanos bus.jpg', '', '', '', '', '', '', '', ''),
(455, 2, 17, 'Marco vertical', '', '1473', '', 0, '', 'D3YS.1473 marco vertical carroceria.jpg', '', '', '', '', '', '', '', ''),
(456, 2, 17, 'Platina piso pajaro azul', '', '1469', '', 0, '', '1XH.1469 platina piso pajaro azul.jpg', '', '', '', '', '', '', '', ''),
(457, 2, 17, 'Marco ventanería', '', '2144', '', 0, '', 'IMCB.2144 marco ventaneria.jpg', '', '', '', '', '', '', '', ''),
(458, 2, 16, 'Canal con aleta', '', '2904', '', 0, '2\\" x 1\\"', 'MM8X.2904 Canal con aleta desc.jpg', '', '', '', '', '', '', '', ''),
(459, 2, 16, 'Canal con aleta corrida', '', '2666', '', 0, '', 'ZN8N.2666 canal con aleta.jpg', '', '', '', '', '', '', '', ''),
(460, 2, 16, 'Canal con aleta', '', '2899', '', 0, '3\\" x 1\\"', 'QL6T.2899 Canal con aleta desc.jpg', '', '', '', '', '', '', '', ''),
(461, 2, 16, 'Canal con aleta', '', '2473', '', 0, '', 'F8JB.2473 Canal con aleta.jpg', '', '', '', '', '', '', '', ''),
(462, 2, 16, 'Canal con aleta', '', '2084', '', 0, '3\\" x 1 1/2\\"', 'MNA.2084 Canal con aleta desc.jpg', '', '', '', '', '', '', '', ''),
(463, 2, 16, 'Canal con aleta', '', '1508', '', 0, '4\\"  x  2  9/32\\"', 'FHGZ.1508 canal con aleta desc.jpg', '', '', '', '', '', '', '', ''),
(464, 2, 16, 'Canal liso', '', '1769', '', 0, '', 'BEH3.1769 Canal Lisa.jpg', '', '', '', '', '', '', '', ''),
(465, 2, 16, 'Canal lisa', '', '1773', '', 0, '', 'YY3H.1773 Canal Lisa.jpg', '', '', '', '', '', '', '', ''),
(466, 2, 16, 'Canal lisa', '', '1510', '', 0, '', '6XU1.1510 Canal Liso.jpg', '', '', '', '', '', '', '', ''),
(467, 2, 16, 'Terminal remate en C', '', '2026', '', 0, '', 'RI83.2026 C terminal remate.jpg', '', '', '', '', '', '', '', ''),
(468, 2, 16, 'Canal de 25 x 15 mm', '', '2098', '', 0, '', 'BDVJ.2098 canal de 25 x 15.jpg', '', '', '', '', '', '', '', ''),
(469, 2, 16, 'Marco puerta entamborada', '', '2101', '', 0, '', 'KNBM.2101 marco puerta entamborada.jpg', '', '', '', '', '', '', '', ''),
(470, 2, 16, 'Canal lisa', '', '2079', '', 0, '', 'KB17.2079 Canal Lisa.jpg', '', '', '', '', '', '', '', ''),
(471, 2, 4, 'Bisagra universal', '', '2477', '', 0, '', 'U3TP.2477 bisagra universal.jpg', '', '', '', '', '', '', '', ''),
(472, 2, 4, 'Bisagra división de baño', '', '2543', '', 0, '', 'U5TW.2543 bisagra division de baño.jpg', '', '', '', '', '', '', '', ''),
(473, 2, 4, 'Bisagra de 50.8 mm', '', '2340', '', 0, '', 'MSMI.2340 bisagra 50.8.jpg', '', '', '', '', '', '', '', ''),
(474, 2, 4, 'Bisagra de 53.2 mm', '', '2096', '', 0, '', 'HJR8.2096 bisagra de 53.2.jpg', '', '', '', '', '', '', '', ''),
(475, 2, 60, 'Sillar alfajía', '', '2409', '', 0, '', 'ZXQJ.2409 Sillar alfajia.jpg', '', '', '', '', '', '', '', ''),
(477, 2, 15, 'Ángulo de 1/2\\" x 1\\" ', '', '1280', '', 0, 'Ángulos', '8Y2L.angulos de aletas desiguales.jpg', '', '', '', '', '', '', '', ''),
(478, 2, 15, ' Ángulo de 4 mm', '', '1641', '', 0, 'Ángulos', 'UJ3C.angulos de aletas desiguales.jpg', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_products_aspect`
--

CREATE TABLE IF NOT EXISTS `cms_products_aspect` (
  `products_aspect_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `products_aspect_title` varchar(80) default NULL COMMENT 'title of the field',
  `products_aspect_description` varchar(120) default NULL COMMENT 'description for the user',
  `products_aspect_value` text COMMENT 'field value',
  `products_aspect_type` varchar(20) default NULL COMMENT 'field type',
  PRIMARY KEY  (`products_aspect_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_products_cat`
--

CREATE TABLE IF NOT EXISTS `cms_products_cat` (
  `products_cat_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `products_cat_title` varchar(120) default NULL COMMENT 'cat title',
  PRIMARY KEY  (`products_cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `cms_products_cat`
--

INSERT INTO `cms_products_cat` (`products_cat_id`, `products_cat_title`) VALUES
(1, 'Línea Universal'),
(2, 'Línea Tradicional');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_products_config`
--

CREATE TABLE IF NOT EXISTS `cms_products_config` (
  `products_config_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `products_config_funcionality` int(1) NOT NULL COMMENT 'Funcionalidad del módulo',
  `products_config_date` datetime default NULL COMMENT 'Fecha y hora de instalación módulo',
  PRIMARY KEY  (`products_config_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `cms_products_config`
--

INSERT INTO `cms_products_config` (`products_config_id`, `products_config_funcionality`, `products_config_date`) VALUES
(1, 1, '2012-09-27 10:50:24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_products_subcat`
--

CREATE TABLE IF NOT EXISTS `cms_products_subcat` (
  `products_subcat_id` int(11) unsigned NOT NULL auto_increment COMMENT 'id unico',
  `products_subcat_id_cat` int(11) unsigned NOT NULL COMMENT 'id cat',
  `products_subcat_title` varchar(120) default NULL COMMENT 'subcat title',
  PRIMARY KEY  (`products_subcat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=63 ;

--
-- Volcado de datos para la tabla `cms_products_subcat`
--

INSERT INTO `cms_products_subcat` (`products_subcat_id`, `products_subcat_id_cat`, `products_subcat_title`) VALUES
(1, 2, 'Barras solidas'),
(2, 2, 'Alfajías'),
(4, 2, 'Bisagras'),
(5, 2, 'Avisos publicitarios'),
(6, 1, 'PC Astral 2.0'),
(7, 1, 'PC Colosal 2.6'),
(8, 1, 'PB Sideral 2.4'),
(9, 1, 'PC Colosal 345'),
(10, 1, 'VB Boreal 1.8'),
(11, 1, 'VB Boreal 1.8 Plus'),
(12, 1, 'VC Astral 1.6'),
(13, 1, 'VC Astral 1.7'),
(14, 1, 'VC Astral 1.8'),
(15, 2, 'Ángulos'),
(16, 2, 'Canales'),
(17, 2, 'Carrocería'),
(18, 2, 'División de baño'),
(19, 2, 'División de interiores'),
(20, 2, 'División de ferias'),
(21, 2, 'Ductos'),
(22, 2, 'Enchapes'),
(23, 2, 'Escaleras'),
(24, 2, 'Fachada flotante'),
(25, 2, 'Mamparas'),
(26, 2, 'Manijas y malayos'),
(28, 2, 'Pasamanos'),
(29, 2, 'Peinazos'),
(30, 2, 'Perfiles en C'),
(31, 2, 'Perfiles en H'),
(32, 2, 'Perfiles en T'),
(33, 2, 'Perfiles en U'),
(34, 2, 'Perfiles varios'),
(35, 2, 'Persianeros'),
(36, 2, 'Pirlanes'),
(37, 2, 'Platinas'),
(38, 2, 'Puerta Batiente Económica'),
(39, 2, 'Puerta Corrediza 2025'),
(40, 2, 'Puerta Corrediza 2027'),
(41, 2, 'Puerta Corrediza 2028 Plus'),
(42, 2, 'Puerta Corrediza 2044'),
(43, 2, 'Puerta entamborada'),
(44, 2, 'Rejas y balcones'),
(45, 2, 'Sistema 2x1 pulgada'),
(46, 2, 'Sistema 3x1 1/2 pulgadas'),
(47, 2, 'Sistema 3x1 pulgadas'),
(48, 2, 'Sistema 4x1 3/4 pulgadas'),
(49, 2, 'Tubería de riego'),
(50, 2, 'Tubos acanalados'),
(51, 2, 'Tubos circulares'),
(52, 2, 'Tubos cuadrados'),
(53, 2, 'Tubos de cortinería y estriados'),
(54, 2, 'Tubos esquineros'),
(55, 2, 'Tubos rectangulares'),
(56, 2, 'Tubulares varios'),
(57, 2, 'Ventana corrediza 4020'),
(58, 2, 'Ventana corrediza 4025'),
(59, 2, 'Ventana corrediza 4026'),
(60, 2, 'Ventana proyectante'),
(61, 2, 'Zócalos'),
(62, 2, 'Sistema de vitrinas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_user`
--

CREATE TABLE IF NOT EXISTS `cms_user` (
  `id_user` int(11) unsigned NOT NULL auto_increment,
  `username_user` varchar(60) default NULL,
  `password_user` varchar(100) default NULL,
  `email_user` varchar(50) default NULL,
  `rol_user` varchar(11) default NULL,
  PRIMARY KEY  (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `cms_user`
--

INSERT INTO `cms_user` (`id_user`, `username_user`, `password_user`, `email_user`, `rol_user`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'cms@imaginamos.com', 'admin');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
