<?php
session_start();
include 'cms/core/class/db.class.php';

include 'cms/modules/galeria/model/cat.php';
include 'cms/modules/galeria/model/catDAO.php';
include 'cms/modules/galeria/model/galeria.class.php';
include 'cms/modules/galeria/model/galeriaDAO.class.php';

$db = new Database();
$db->connect();


$catDAO = new CatDAO($db);
$cats = $catDAO->gets("galeria_cats_name", "asc");

$galeriaDAO = new GaleriaDAO($db);
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="ie6 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="ie7 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="ie8 lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="ie9"> <!--<![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>VITRAL</title>
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <meta name="keywords" content="Aluminio, Ventanas, Puertas, Perfiles, Perfilería, Vitral, Línea+universal, Corrediza, Corredera, Proyectante, Batiente">
    <script language="javascript" type="text/javascript" src="js/jquery-1.7.2.min.js"> </script> 
    <!-- CUSTOM SELECT http://jquery.sanchezsalvador.com/page/jquerycombobox.aspx-->
    <script language="javascript" type="text/javascript" src="js/jquery.dd.js"> </script> 
    <link href="css/dd.css" rel="stylesheet" type="text/css">
    <!-- ColorBox-->
    <link rel="stylesheet" href="css/colorbox.css" />
    <script src="js/jquery.colorbox.js"></script>
    <!-- CUSTOM SCROLLBAR-->
    <script language="javascript" type="text/javascript" src="js/jquery.mousewheel.js"> </script> 
    <script language="javascript" type="text/javascript" src="js/jquery.jscrollpane.min.js"> </script>
    <link rel="stylesheet" type="text/css" href="css/jquery.jscrollpane.css">
    <!--js personales -->
    <script language="javascript" type="text/javascript" src="js/vitral.js"> </script> 
    <script>
	$(document).ready(function(){
		//Examples of how to assign the ColorBox event to elements
		$(".group1").colorbox({rel:'group1'});
		$(".group2").colorbox({rel:'group2'});
		$(".group3").colorbox({rel:'group3'});
		$(".group4").colorbox({rel:'group4'});
		$(".group5").colorbox({rel:'group5'});
		//Inicializa Scroll
		$('.albumBox').jScrollPane({showArrows:true});
	})
	</script>
    <link href="css/vitral.css" rel="stylesheet" type="text/css">
</head>

<body>
<div class="headerWrapper">
	<div class="headderBox">
    	<div class="logoBox"><a href="index.php"><img src="imagenes/logo.png" width="211" height="113" border="0" /></a></div>
        <div class="sloganBox">Perfiles y sistemas<br />con <strong>CALIDAD DE VIDA</strong></div>
        
        <div class="headerMenuBox">
        	<a href="index.php">HOME</a>
        	<a href="quienesSomos.php">QUIÉNES SOMOS</a>
            <a href="clubClientes.php">DESCARGAS</a> 
            <a href="productos.php">PRODUCTOS</a>    
            <a href="beneficios.php">BENEFICIOS Y NOTICIAS</a> 
            <a class="selected" href="galeria.php">GALERÍA DE IMAGENES</a>
            <div class="logoSegundo"><a href="alma.php"><img src="imagenes/almaLogo.png" width="85" height="46" border="0"  /></a></div>
        </div>
        
        <div class="contactenosBox">
        	<span class="contactenosLeyenda">"Somos el respaldo a su <strong>VENTA</strong>"</span>
            <a href="contactenos.php">CONTÁCTENOS</a>
        </div>
        
        <div class="buscadorBox">
        	<?php include 'busqueda.php';?>
        </div>
    </div>
</div>
<div class="contentWrapper">
	<div class="contentBox">
    	<div class="internasBox">
    		<h1>GALERIA DE IMAGENES</h1><br />

            <?php $i=1; foreach($cats as $cat){ $pics = $galeriaDAO->getsByCat($cat->getId(), "id", "desc"); ?>
                <div class="tituloAlbum" id="albumTitulo<?php echo $i;?>"><?php echo strtoupper($cat->getName());?></div>
          	<div class="albumBox" id="album<?php echo $i;?>">
            	<div class="albumContainer">
                    <?php foreach($pics as $pic){?>
                    <a href="cms/modules/galeria/files/<?php echo $pic->getFile();?>" class="albumItemBox group1">
                        <img src="cms/modules/galeria/files/<?php echo $pic->getFile();?>" width="170" height="240" border="0" alt=""/>
                        <span class="infoBox"><p><?php echo $pic->getTitle();?></p></span>
                    </a>
                    <?php } ?>
                </div>
            </div>
            <?php  $i++;} ?>
            
            </div>
      	</div>
    </div>
    <div class="contentBelowBg"></div>	
</div>

<?php include('footer.php'); ?>
</body>
<?php include 'analytics.php';?>
</html>
