<?php
session_start();
include 'cms/core/class/db.class.php';

include 'cms/modules/nosotros/define.php';
include 'cms/modules/nosotros/model/aspect.class.php';
include 'cms/modules/nosotros/model/aspectDAO.class.php';


$db = new Database();
$db->connect();


$DAO = new AspectDAO($db);
$nosotros = $DAO->getByName('Nosotros');
$valores = $DAO->getByName('Valores');
$mision = $DAO->getByName('Misión y Visión');
$propuesta = $DAO->getByName('Propuesta de Valor');
$politica1 = $DAO->getByName('Política de Calidad');
$politica2 = $DAO->getByName('Política de Venta');
$materia = $DAO->getByName('Materia Prima');
$presentacion = $DAO->getByName('Presentación');
$acabados = $DAO->getByName('Acabados');
$calidad = $DAO->getByName('Calidad');
$imagen1 = $DAO->getByName('Imagen 1');
$imagen2 = $DAO->getByName('Imagen 2');
$imagen3 = $DAO->getByName('Imagen 3');

$proceso = $DAO->getByName('proceso de fabricacion');

?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="ie6 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="ie7 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="ie8 lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="ie9"> <!--<![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>VITRAL</title>
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <meta name="keywords" content="Aluminio, Ventanas, Puertas, Perfiles, Perfilería, Vitral, Línea+universal, Corrediza, Corredera, Proyectante, Batiente">
    <script language="javascript" type="text/javascript" src="js/jquery-1.7.2.min.js"> </script> 
    <!-- CUSTOM SELECT http://jquery.sanchezsalvador.com/page/jquerycombobox.aspx-->
    <script language="javascript" type="text/javascript" src="js/jquery.dd.js"> </script> 
    <link href="css/dd.css" rel="stylesheet" type="text/css">
    <!--js personales -->
    <script language="javascript" type="text/javascript" src="js/vitral.js"> </script> 
    
    <link href="css/vitral.css" rel="stylesheet" type="text/css">
</head>

<body>
<div class="headerWrapper">
	<div class="headderBox">
    	<div class="logoBox"><a href="index.php"><img src="imagenes/logo.png" width="211" height="113" border="0" /></a></div>
        <div class="sloganBox">Perfiles y sistemas<br />con <strong>CALIDAD DE VIDA</strong></div>
        
        <div class="headerMenuBox">
        	<a href="index.php">HOME</a>
        	<a class="selected" href="quienesSomos.php">QUIÉNES SOMOS</a>
            <a href="clubClientes.php">DESCARGAS</a> 
            <a href="productos.php">PRODUCTOS</a>    
            <a href="beneficios.php">BENEFICIOS Y NOTICIAS</a> 
            <a href="galeria.php">GALERÍA DE IMAGENES</a>
            <div class="logoSegundo"><a href="alma.php"><img src="imagenes/almaLogo.png" width="85" height="46" border="0"  /></a></div>
        </div>
        
        <div class="contactenosBox">
        	<span class="contactenosLeyenda">"Somos el respaldo a su <strong>VENTA</strong>"</span>
            <a href="contactenos.php">CONTÁCTENOS</a>
        </div>
        
        <div class="buscadorBox">
        	<?php include 'busqueda.php';?>
        </div>
    </div>
</div>
<div class="contentWrapper">
	<div class="contentBox">
    	<div class="internasBox">
    		<h1>QUIÉNES SOMOS </h1>
            <div class="halfCol" id="quienesSomosItems">
            
            	<div class="quienesSomosItem quienesSomosSelected">
                	<div class="titulo">NOSOTROS</div>
                        <div class="text"><?php echo $nosotros->getValue();?></div>
                </div>
                
                <div class="quienesSomosItem">
                	<div class="titulo">VALORES</div>
                    <div class="text"><?php echo $valores->getValue();?></div>
                </div>
                <div class="quienesSomosItem">
                	<div class="titulo">MISIÓN Y VISIÓN</div>
                    <div class="text"><?php echo $mision->getValue();?></div>
                </div>
                <div class="quienesSomosItem">
                	<div class="titulo">PROPUESTA DE VALOR</div>
                    <div class="text"><?php echo $propuesta->getValue();?></div>
                </div>
                <div class="quienesSomosItem">
                	<div class="titulo">POLÍTICA DE CALIDAD</div>
                    <div class="text"><?php echo $politica1->getValue();?></div>
                </div>
                <div class="quienesSomosItem">
                	<div class="titulo">POLÍTICA DE VENTAS</div>
                    <div class="text"><?php echo $politica2->getValue();?></div>
                </div>
                <div class="quienesSomosItem">
                	<div class="titulo">MATERIA PRIMA</div>
                    <div class="text"><?php echo $materia->getValue();?></div>
                </div>
                <div class="quienesSomosItem">
                	<div class="titulo">PROCESO DE FABRICACIÓN</div>
                    <div class="text"><?php echo $proceso->getValue();?></div>
                </div>
                <div class="quienesSomosItem">
                	<div class="titulo">PRESENTACIÓN</div>
                    <div class="text"><?php echo $presentacion->getValue();?></div>
                </div>
                <div class="quienesSomosItem">
                	<div class="titulo">ACABADOS</div>
                    <div class="text"><?php echo $acabados->getValue();?></div>
                </div>
                <div class="quienesSomosItem">
                	<div class="titulo">CALIDAD</div>
                    <div class="text"><?php echo $calidad->getValue();?></div>
                </div>
                
            </div>
            <div class="halfCol">
                <img style="margin-bottom:20px;" src="cms/modules/nosotros/files/<?php echo $imagen1->getValue();?>" width="438" height="230" />
                <img style="margin-bottom:20px;" src="cms/modules/nosotros/files/<?php echo $imagen2->getValue();?>" width="438" height="230" />
                <img style="margin-bottom:20px;" src="cms/modules/nosotros/files/<?php echo $imagen3->getValue();?>" width="438" height="230" />
            </div>
            <div class="clear"></div>
      	</div>
    </div>
    <div class="contentBelowBg"></div>	
</div>

<?php include('footer.php'); ?>
</body>
<?php include 'analytics.php';?>
</html>