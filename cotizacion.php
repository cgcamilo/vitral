<?php
session_start();
include 'cms/core/class/db.class.php';

include 'cms/modules/products/define.php';
include 'cms/modules/products/model/category.php';
include 'cms/modules/products/model/categoryDAO.php';
include 'cms/modules/products/model/subCategoryDAO.php';
include 'cms/modules/products/model/product.class.php';
include 'cms/modules/products/model/productDAO.class.php';


$db = new Database();
$db->connect();

if( !isset($_SESSION['ids']) ){
    $array = array();
    $_SESSION['ids'] = serialize($array);
}


$array = unserialize($_SESSION['ids']);

if( isset($_GET['id']) ){
    unset($array[$_GET['id']]);
    $_SESSION['ids'] = serialize($array);
}

if( count($array) == 0 ){
    $location = "location: ./productos.php?";
    header($location."&message=Tienes que añadir productos si deseas cotizarlos");
    exit;
}

$DAO = new ProductDAO($db);


?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="ie6 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="ie7 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="ie8 lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="ie9"> <!--<![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>VITRAL</title>
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    
    <script language="javascript" type="text/javascript" src="js/jquery-1.7.2.min.js"> </script> 
    <!-- CUSTOM SELECT http://jquery.sanchezsalvador.com/page/jquerycombobox.aspx-->
    <script language="javascript" type="text/javascript" src="js/jquery.dd.js"> </script> 
    <link href="css/dd.css" rel="stylesheet" type="text/css">
    <!--js personales -->
    <script language="javascript" type="text/javascript" src="js/vitral.js"> </script> 
    <script>
	$(document).ready(function() {
		/*CONTÁCTENOS*/
		$(".customSelect2").msDropDown({mainCSS:"dd2"});
    });
	</script>
    <link href="css/vitral.css" rel="stylesheet" type="text/css">


    <?php if( isset($_GET['message']) ){ ?>
    <script type="text/javascript">
        alert("<?php echo $_GET['message']; ?>");
    </script>
    <?php } ?>
</head>

<body>
<div class="headerWrapper">
	<div class="headderBox">
    	<div class="logoBox"><a href="index.php"><img src="imagenes/logo.png" width="211" height="113" border="0" /></a></div>
        <div class="sloganBox">Perfiles y sistemas<br />con <strong>CALIDAD DE VIDA</strong></div>
        
        <div class="headerMenuBox">
        	<a href="index.php">HOME</a>
        	<a href="quienesSomos.php">QUIÉNES SOMOS</a>
            <a href="clubClientes.php">DESCARGAS</a> 
            <a class="selected" href="productos.php">PRODUCTOS</a>    
            <a href="beneficios.php">BENEFICIOS Y NOTICIAS</a> 
            <a href="galeria.php">GALERÍA DE IMAGENES</a>
            <div class="logoSegundo"><a href="alma.php"><img src="imagenes/almaLogo.png" width="85" height="46" border="0"  /></a></div>
        </div>
        
        <div class="contactenosBox">
        	<span class="contactenosLeyenda">"Somos el respaldo a su <strong>VENTA</strong>"</span>
            <a href="contactenos.php">CONTÁCTENOS</a>
        </div>
        
        <div class="buscadorBox">
        	<?php include 'busqueda.php';?>
        </div>
    </div>
</div>
<div class="contentWrapper">
	<div class="contentBox">
    	<div class="internasBox" style="position:relative">
    		<h1>VER Y ENVIAR COTIZACIÓN</h1><br />
                <span style="color: red"></span>
            <form action="cotizacionAction.php" method="get">
            <strong>Productos Seleccionados</strong>
                <?php foreach( $array as $id ){
                    $p = $DAO->getById($id);
                    if($p == null) continue;
                ?>
                <div class="productosSelectBox">
                    <div class="productosSelectItem">
                        <div class="imagen"><img src="cms/modules/products/files/<?php echo $p->getImg1();?>" width="145" height="97" /></div>
                        <div class="texto">
                            <span class="tituloProducto"><?php echo $p->getTitle();?></span><br />
                            <?php echo $p->getDecription();?><br />
                            <span class="strong">REFERENCIA:</span> <span class="strongRef"><?php echo $p->getRef();?></span>
                            <br />
                            <span class="strong">CANTIDAD DE PAQUETES:</span>
                            <span class="strongRef"><input type="text" name="cantidad<?php echo $p->getId();?>" value="1" size="2" />
                            </span>
                        </div>
                        <div class="deleteBox">
                            <a href="cotizacion.php?id=<?php echo $p->getId();?>">ELIMINAR</a>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <?php } ?>
            <br /><br />

            
            <strong>Información Personal</strong><br /><br />
            
            <div class="halfCol">
                <input type="text"  name="nombres" class="pedido" value="Nombres" onblur="javascript:if(this.value=='') this.value='Nombres';" onfocus="javascript:if(this.value=='Nombres') this.value='';"/>
                <input type="text"  name="apellidos" class="pedido" value="Apellidos" onblur="javascript:if(this.value=='') this.value='Apellidos';" onfocus="javascript:if(this.value=='Apellidos') this.value='';" />
                <input type="text"  name="email" class="pedido" value="E-mail" onblur="javascript:if(this.value=='') this.value='E-mail';" onfocus="javascript:if(this.value=='E-mail') this.value='';" />
                <input type="text"  name="empresa" class="pedido" value="Empresa" onblur="javascript:if(this.value=='') this.value='Empresa';" onfocus="javascript:if(this.value=='Empresa') this.value='';" />
            </div>
            <div class="halfCol">
                <input type="text"  name="tel" class="pedido" value="Teléfono" onblur="javascript:if(this.value=='') this.value='Teléfono';" onfocus="javascript:if(this.value=='Teléfono') this.value='';"  />
                <input type="text"  name="dir" class="pedido" value="Dirección" onblur="javascript:if(this.value=='') this.value='Dirección';" onfocus="javascript:if(this.value=='Dirección') this.value='';"/>
                <input type="text"  name="ciudad" class="pedido" value="Ciudad" onblur="javascript:if(this.value=='') this.value='Ciudad';" onfocus="javascript:if(this.value=='Ciudad') this.value='';" />
            </div>
            
            <div class="clear"></div>
            <div class="productosSelectedEnviarBox">
            	<input type="submit" class="pedido" value="PEDIR COTIZACIÓN" />
            </div>
            </form>
      	</div>
    </div>
    <div class="contentBelowBg"></div>	
</div>

<?php include('footer.php'); ?>
</body>
<?php include 'analytics.php';?>
</html>
