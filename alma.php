<?php
session_start();
include 'cms/core/class/db.class.php';

include 'cms/modules/alma/define.php';
include 'cms/modules/alma/model/aspect.class.php';
include 'cms/modules/alma/model/aspectDAO.class.php';

include 'cms/modules/almanews/define.php';
include 'cms/modules/noticias/model/noticia.php';
include 'cms/modules/almanews/model/noticiaDAOext.php';


$db = new Database();
$db->connect();


$DAO = new AspectDAO($db);
$datos1 = $DAO->getByName('Datos de Contacto');
$datos2 = $DAO->getByName('Datos introductorios');
$imagen1 = $DAO->getByName('Recuadro 1 Imagen');
$r1 = $DAO->getByName('Recuadro 1 título');
$imagen2 = $DAO->getByName('Recuadro 2 Imagen');
$r2 = $DAO->getByName('Recuadro 2 título');
$imagen3 = $DAO->getByName('Recuadro 3 Imagen');
$r3 = $DAO->getByName('Recuadro 3 título');
$imagen4 = $DAO->getByName('Recuadro 4 Imagen');
$r4 = $DAO->getByName('Recuadro 4 título');

$itemsPerPage = 10;
$page = 0;
if( isset($_GET['page']) )
    $page = $_GET['page'];

$DAO = new NoticiaDAOext($db);
$noticias = $DAO->gets("date", "desc", $page*$itemsPerPage, $itemsPerPage);
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="ie6 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="ie7 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="ie8 lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="ie9"> <!--<![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>VITRAL</title>
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <meta name="keywords" content="Aluminio, Ventanas, Puertas, Perfiles, Perfilería, Vitral, Línea+universal, Corrediza, Corredera, Proyectante, Batiente">
    <script language="javascript" type="text/javascript" src="js/jquery-1.7.2.min.js"> </script> 
    <!-- CUSTOM SELECT http://jquery.sanchezsalvador.com/page/jquerycombobox.aspx-->
    <script language="javascript" type="text/javascript" src="js/jquery.dd.js"> </script> 
    <link href="css/dd.css" rel="stylesheet" type="text/css">
    <!--js personales -->
    <script language="javascript" type="text/javascript" src="js/vitral.js"> </script> 
    
    <link href="css/vitral.css" rel="stylesheet" type="text/css">
</head>

<body>
<div class="headerWrapper">
	<div class="headderBox">
    	<div class="logoBox"><a href="index.php"><img src="imagenes/logo.png" width="211" height="113" border="0" /></a></div>
        <div class="sloganBox">Perfiles y sistemas<br />con <strong>CALIDAD DE VIDA</strong></div>
        
        <div class="headerMenuBox">
        	<a href="index.php">HOME</a>
        	<a href="quienesSomos.php">QUIÉNES SOMOS</a>
            <a href="clubClientes.php">DESCARGAS</a> 
            <a href="productos.php">PRODUCTOS</a>    
            <a href="beneficios.php">BENEFICIOS Y NOTICIAS</a> 
            <a href="galeria.php">GALERÍA DE IMAGENES</a>
            <div class="logoSegundo"><a href="alma.php"><img src="imagenes/almaLogo.png" width="85" height="46" border="0"  /></a></div>
        </div>
        
        <div class="contactenosBox">
        	<span class="contactenosLeyenda">"Somos el respaldo a su <strong>VENTA</strong>"</span>
            <a href="contactenos.php">CONTÁCTENOS</a>
        </div>
        
        <div class="buscadorBox">
        	<?php include 'busqueda.php';?>
        </div>
    </div>
</div>
<div class="contentWrapper">
	<div class="contentBox">
    	<div class="internasBox">
    		<h1>ALMA</h1><br />
            
            <div class="clubContentBox" style="margin-bottom:40px;">
            
            	<a   class="clubItemBox" style="margin-right:12px;">
                    <div class="titulo" style="background-image:none;padding-left: 20px; width: 180px;"><p><?php echo $r1->getValue();?></p></div>
                    <img src="cms/modules/alma/files/<?php echo $imagen1->getValue();?>" width="220" height="260" />
                </a>
                
            	<a class="clubItemBox" style="margin-right:12px;">
                	<div class="titulo" style="background-image:none;padding-left: 20px; width: 180px;"><p><?php echo $r2->getValue();?></p></div>
                    <img src="cms/modules/alma/files/<?php echo $imagen2->getValue();?>" width="220" height="260" />
                </a>
                
            	<a  class="clubItemBox" style="margin-right:12px;">
                	<div class="titulo" style="background-image:none;padding-left: 20px; width: 180px;"><p><?php echo $r3->getValue();?></p></div>
                    <img src="cms/modules/alma/files/<?php echo $imagen3->getValue();?>" width="220" height="260" />
                </a>
                
            	<a  class="clubItemBox" style="margin-right:12px;">
                	<div class="titulo" style="background-image:none;padding-left: 20px; width: 180px;"><p><?php echo $r4->getValue();?></p></div>
                    <img src="cms/modules/alma/files/<?php echo $imagen4->getValue();?>" width="220" height="260" />
                </a>
            </div>
			
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:30px">
  <tr>
    <td width="53%" style="padding-right:40px">
        <?php echo nl2br($datos2->getValue());?>
    </td>
    <td width="47%">
        <strong>DATOS DE CONTACTO</strong>
        <br />
        <?php echo nl2br($datos1->getValue());?>
</td>
  </tr>
</table>

            
            
            
          	<?php foreach($noticias as $noticia){ ?>
            <div class="beneficiosItemBox">
                <div class="imageBox">
                    <img src="cms/modules/almanews/files/<?php echo $noticia->getImage1();?>" width="205" height="135" alt="" />
                    <div class="fechaDia"><?php echo $noticia->getDay();?></div>
                    <div class="fechaMes"><?php echo $noticia->getMonth();?></div>
                </div>
                <div class="textos">
                    <h2><?php echo $noticia->getTitle();?></h2>
                    <?php echo $noticia->getDescriptionLimited(350);?>
                    <br /> <br />
                    <a href="almaDetalle.php?id=<?php echo $noticia->getId();?>" class="vermasNoticias">VER MÁS</a>
                </div>
                <div class="clear"></div>
            </div>
            <?php } ?>
            
            
            <div class="clear"></div>
      	</div>
    </div>
    <div class="contentBelowBg"></div>	
</div>

<?php include('footer.php'); ?>
</body>
<?php include 'analytics.php';?>
</html>
