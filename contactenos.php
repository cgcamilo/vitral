<?php
session_start();
include 'cms/core/class/db.class.php';

include 'cms/modules/products/define.php';
include 'cms/modules/products/model/category.php';
include 'cms/modules/products/model/categoryDAO.php';
include 'cms/modules/products/model/subCategoryDAO.php';
include 'cms/modules/products/model/product.class.php';
include 'cms/modules/products/model/productDAO.class.php';
include 'mail.php';

$db = new Database();
$db->connect();

//cats
$catDAO = new categoryDAO($db);
$cats = $catDAO->gets("products_cat_title", "asc");


$erro = 0;
if( isset($_GET['nombres']) ){
    
    foreach($_GET as $key => $value){
        if( $value == "" )
            $error = 1;
        $$key = $value;
    }

    if( $nombre == 'Nombre' || $apellidos == "Apellidos" || $email == "E-mail" || $empresa == "Empresa"
        || $interes == "Interés" || $corta == "Comentarios"){
        $error = 1;
    }

    if( !ValidaMail($email) ){
        $error = 3;
    }

    if( $error == 0 ){
        $body = "<h3>Mensaje de contacto:</h3>\n\n";
        $body .= "Un usuario un mensaje desde la web:.<br />\n";

        $body .= "<h4>Datos cliente:</43><br />\n";
        $body .= "<b>Nombre:</b> ".accents2HTML($nombres)."<br />\n";
        $body .= "<b>Apellidos:</b> ".accents2HTML($apellidos)."<br />\n";
        $body .= "<b>Email:</b> ".accents2HTML($email)."<br />\n";
        $body .= "<b>Empresa:</b> ".accents2HTML($empresa)."<br />\n";
        $body .= "<b>Interes:</b> ".accents2HTML($interes)."<br />\n";
        $body .= "<b>Comentario:</b> ".accents2HTML($corta)."<br />\n";

        $mail2 = new sendCMail("servicioalcliente@vitral.com.co", "system@imaginamos.com", "Contacto Vitral", $body, "text/html");
      
        $mail2->sendMail();

        $error = 2;
    }
}

?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="ie6 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="ie7 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="ie8 lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="ie9"> <!--<![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>VITRAL</title>
    <meta name="keywords" content="Aluminio, Ventanas, Puertas, Perfiles, Perfilería, Vitral, Línea+universal, Corrediza, Corredera, Proyectante, Batiente">
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <?php if( $error == 1 ){ ?>
    <script type="text/javascript">
        alert("Todos los datos son obligatorios. Gracias");
    </script>
    <?php } ?>
        <?php if( $error == 3 ){ ?>
    <script type="text/javascript">
        alert("Email invalido");
    </script>
    <?php } ?>
     <?php if( $error == 2 ){ ?>
    <script type="text/javascript">
        alert("Mensaje enviado correctamente");
    </script>
    <?php } ?>
    <script language="javascript" type="text/javascript" src="js/jquery-1.7.2.min.js"> </script> 
    <!-- CUSTOM SELECT http://jquery.sanchezsalvador.com/page/jquerycombobox.aspx-->
    <script language="javascript" type="text/javascript" src="js/jquery.dd.js"> </script> 
    <link href="css/dd.css" rel="stylesheet" type="text/css">
    <!--js personales -->
    <script language="javascript" type="text/javascript" src="js/vitral.js"> </script> 
    <script>
	$(document).ready(function() {
		/*CONTÁCTENOS*/
		$(".customSelect").msDropDown().data("dd");
    });
	</script>
    <link href="css/vitral.css" rel="stylesheet" type="text/css">
    <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script> 
<script type="text/javascript"> 
    $(document).ready(function() { initialize(); });

    function initialize() {
        var map_options = {
            center: new google.maps.LatLng(3.404186,-76.523037),
            zoom: 6,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var google_map = new google.maps.Map(document.getElementById("map_canvas"), map_options);

        var info_window = new google.maps.InfoWindow({
            content: 'loading'
        });

        var t = [];
        var x = [];
        var y = [];
        var h = [];

        t.push('Vitral Cali');
        x.push(3.402987);
        y.push(-76.523375);
        h.push('<p><strong>Vitral Cali</strong><br>Cll 25 No. 5 – 44 <br>Tel: (57–2)882 2694</p>');

        t.push('Vitral Bogotá');
        x.push(4.606516);
        y.push(-74.124992);
        h.push('<p><strong>Vitral Bogotá</strong><br>Cra 52 No. 79 - 20 <br>Tel:(57-1)311 6400</p>');

        var i = 0;
        for ( item in t ) {
            var m = new google.maps.Marker({
                map:       google_map,
                animation: google.maps.Animation.DROP,
                title:     t[i],
                position:  new google.maps.LatLng(x[i],y[i]),
                html:      h[i]
            });

            google.maps.event.addListener(m, 'click', function() {
                info_window.setContent(this.html);
                info_window.open(google_map, this);
            });
            i++;
        }
    }
</script> 
</head>

<body>
<div class="headerWrapper">
	<div class="headderBox">
    	<div class="logoBox"><a href="index.php"><img src="imagenes/logo.png" width="211" height="113" border="0" /></a></div>
        <div class="sloganBox">Perfiles y sistemas de aluminio<br />con <strong>CALIDAD DE VIDA</strong></div>
        
        <div class="headerMenuBox">
        	<a href="index.php">HOME</a>
        	<a href="quienesSomos.php">QUIÉNES SOMOS</a>
            <a href="clubClientes.php">DESCARGAS</a> 
            <a href="productos.php">PRODUCTOS</a>    
            <a href="beneficios.php">BENEFICIOS Y NOTICIAS</a> 
            <a href="galeria.php">GALERÍA DE IMAGENES</a>
            <div class="logoSegundo"><a href="alma.php"><img src="imagenes/almaLogo.png" width="85" height="46" border="0"  /></a></div>
        </div>
        
        <div class="contactenosBox">
        	<span class="contactenosLeyenda">"Somos el respaldo a su <strong>VENTA</strong>"</span>
            <a href="contactenos.php">CONTÁCTENOS</a>
        </div>
        
        <div class="buscadorBox">
        	<?php include 'busqueda.php';?>
        </div>
    </div>
</div>
<div class="contentWrapper">
	<div class="contentBox">
    	<div class="internasBox">
    		<h1>CONTÁCTENOS</h1>
            
            <span class="subtitulo">Bienvenido a nuestra página web; para nosotros es importante recibir sus comentarios. El siguiente formulario tiene como único fin ponernos en contacto para darle respuesta a su solicitud.</span> <br /><br />

            <div class="halfCol">
                <span style="color: red">
                
                </span>
                <form class="contactenosForm" action="contactenos.php" method="get">
                    <input type="text" name="nombres" value="Nombre" onblur="javascript:if(this.value=='') this.value='Nombre';" onfocus="javascript:if(this.value=='Nombre') this.value='';" />
                    <input type="text" name="apellidos" value="Apellidos" onblur="javascript:if(this.value=='') this.value='Apellidos';" onfocus="javascript:if(this.value=='Apellidos') this.value='';"  />
                    <input type="text" name="email" value="E-mail" onblur="javascript:if(this.value=='') this.value='E-mail';" onfocus="javascript:if(this.value=='E-mail') this.value='';"  />
                    <input type="text" name="empresa" value="Empresa" onblur="javascript:if(this.value=='') this.value='Empresa';" onfocus="javascript:if(this.value=='Empresa') this.value='';"  />
                    <select class="customSelect" style="width:99.5%" name="interes">
                        <option>Interés</option>
                        <option>sugerencia</option>
                        <option>reclamo</option>
                        <option>felicitación</option>
                        <option>otro</option>
                    </select>
                	<textarea name="corta" cols="" rows="" onblur="javascript:if(this.value=='') this.value='Comentarios';" onfocus="javascript:if(this.value=='Comentarios') this.value='';" >Comentarios</textarea>
                    <input type="submit" value="ENVIAR" />
                </form>
            </div>
            <div class="halfCol">
            	<div class="mapaBox">
                		<div id="map_canvas" style="width:350px;height:350px;">Google Map</div> 
                </div>
                <div class="direccion"><span>Vitral Cali</span><br>

                        Cll 25 No. 5 – 44 <br>
                        Tel: (57–2)882 2694<br /><br />
                        
                        <span>Vitral Bogotá</span><br>

                        Cra 52 No. 79 - 20 <br />
                        Tel:(57-1)311 6400
            	</div>
            </div>
            <div class="clear"></div>
      	</div>
    </div>
    <div class="contentBelowBg"></div>	
</div>

<?php include('footer.php'); ?>
</body>
<?php include 'analytics.php';?>
</html>
