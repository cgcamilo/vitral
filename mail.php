<?php

/*
 * By Camilo Cifuentes    (  http://www.ccamilo.com/  )
 */

class sendCMail{
    private $to;
    private $from;
    private $body;
    private $subject;
    private $type;

    function __construct($to, $from, $subject, $body, $type = "multipart/mixed"){
        $this->from = $from;
        $this->to = $to;
        $this->subject = $subject;
        $this->body = $body;
        $this->type = $type;
    }

    function sendMail(){

        $headers = "From: $this->from";


        $semi_rand = md5( time() );
        $mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";

        $headers .= "\nMIME-Version: 1.0\n" .
                    "Content-Type: {$this->type};\n" .
                    " boundary=\"{$mime_boundary}\"";

		$message = $this->body;


        if( mail( $this->to, $this->subject, $message, $headers ) )
            return true;
        else
            return false;

    }
}

function ValidaMail($pMail) {
   	if (ereg("^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@+([_a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]{2,200}\.[a-zA-Z]{2,6}$", $pMail ) ) {
      	 return true;
   	} else {
      	 return false;
   	}
}

function accents2HTML($mensaje){
    $mensaje = str_replace("á","&aacute;",$mensaje);
    $mensaje = str_replace("é","&eacute;",$mensaje);
    $mensaje = str_replace("í","&iacute;",$mensaje);
    $mensaje = str_replace("ó","&oacute;",$mensaje);
    $mensaje = str_replace("ú","&uacute;",$mensaje);
    $mensaje = str_replace("ñ","&ntilde;",$mensaje);
    $mensaje = str_replace("¿","&iquest;",$mensaje);

    $mensaje = str_replace("Á","&Aacute;",$mensaje);
    $mensaje = str_replace("É","&Eacute;",$mensaje);
    $mensaje = str_replace("Í","&Iacute;",$mensaje);
    $mensaje = str_replace("Ó","&Oacute;",$mensaje);
    $mensaje = str_replace("Ú","&Uacute;",$mensaje);
    $mensaje = str_replace("Ñ","&Ntilde;",$mensaje);
    return $mensaje;
}
?>
